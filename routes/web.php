<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('vaamUsers', 'VaamUsersController');
Route::post('vaamUsers/store', 'VaamUsersController@store')->name('vaam.store');
Route::get('vaamUsers/edit/{id}', 'VaamUsersController@edit')->name('vaam.edit');
Route::post('vaamUsers/update/{id}', 'VaamUsersController@update')->name('vaam.update');
Route::get('vaamUsers/delete/{id}', 'VaamUsersController@destroy')->name('vaam.delete');

Route::resource('commodity', 'CommodityController');
Route::post('commodity/store', 'CommodityController@store')->name('commodity.store');
Route::get('commodity/edit/{id}', 'CommodityController@edit')->name('commodity.edit');
Route::post('commodity/update/{id}', 'CommodityController@update')->name('commodity.update');
Route::get('commodity/delete/{id}', 'CommodityController@destroy')->name('commodity.delete');


Route::resource('commodity-category', 'CommoditycategoryController');
Route::get('commodity-category/create/{id}', 'CommoditycategoryController@create')->name('commodity.category.create');
Route::post('commodity-category/store', 'CommoditycategoryController@store')->name('commodity.category.store');
Route::get('commodity-category/edit/{id}', 'CommoditycategoryController@edit')->name('commodity.category.edit');
Route::post('commodity-category/update/{id}', 'CommoditycategoryController@update')->name('commodity.category.update');
Route::get('commodity-category/delete/{id}', 'CommoditycategoryController@destroy')->name('commodity.category.delete');

Route::resource('vendor', 'VendorController');
Route::post('vendor/store', 'VendorController@store')->name('vendor.store');
Route::get('vendor/edit/{id}', 'VendorController@edit')->name('vendor.edit');
Route::post('vendor/update/{id}', 'VendorController@update')->name('vendor.update');
Route::get('vendor/delete/{id}', 'VendorController@destroy')->name('vendor.delete');

Route::resource('customer', 'CustomerController');
Route::post('customer/store', 'CustomerController@store')->name('customer.store');
Route::get('customer/edit/{id}', 'CustomerController@edit')->name('customer.edit');
Route::post('customer/update/{id}', 'CustomerController@update')->name('customer.update');
Route::get('customer/delete/{id}', 'CustomerController@destroy')->name('customer.delete');

Route::resource('expanse', 'ExpanseController');
Route::post('expanse/store', 'ExpanseController@store')->name('expanse.store');
Route::get('expanse/edit/{id}', 'ExpanseController@edit')->name('expanse.edit');
Route::post('expanse/update/{id}', 'ExpanseController@update')->name('expanse.update');
Route::get('expanse/delete/{id}', 'ExpanseController@destroy')->name('expanse.delete');

Route::resource('purchase', 'PurchasemgmController');
Route::post('purchase/store', 'PurchasemgmController@store')->name('purchase.store');
Route::get('purchase/edit/{id}', 'PurchasemgmController@edit')->name('purchase.edit');
Route::post('purchase/update/{id}', 'PurchasemgmController@update')->name('purchase.update');
Route::get('purchase/delete/{id}', 'PurchasemgmController@destroy')->name('purchase.delete');
Route::get('purchase/getpurchase/{id}', 'PurchasemgmController@getpurchase')->name('purchase.getpurchase');
Route::get('purchase/getpurchaseVendor/{id}', 'PurchasemgmController@getpurchaseVendor')->name('purchase.getpurchase');
Route::get('purchase/getpurchasecustomer/{id}', 'PurchasemgmController@getpurchasecustomer')->name('purchase.getpurchase');
Route::get('purchase/getpurchasecommodity/{id}', 'PurchasemgmController@getpurchasecommodity')->name('purchase.getpurchasecommodity');
Route::get('purchase/getcomapnyUser/{id}', 'PurchasemgmController@getcomapnyUser')->name('purchase.getcomapnyUser');


Route::resource('sales', 'SalesController');
Route::post('sales/store', 'SalesController@store')->name('sales.store');
Route::get('sales/edit/{id}', 'SalesController@edit')->name('sales.edit');
Route::post('sales/update/{id}', 'SalesController@update')->name('sales.update');
Route::get('sales/delete/{id}', 'SalesController@destroy')->name('sales.delete');
Route::get('sales/getpurchase/{id}', 'SalesController@getpurchase')->name('sales.getpurchase');
Route::get('sales/getpurchasecustomer/{id}', 'SalesController@getpurchasecustomer')->name('sales.getpurchase');
Route::get('sales/getcurrentcommodity/{id}', 'SalesController@getcurrentcommodity')->name('sales.getcurrentcommodity');
Route::get('sales/getCustomer/{id}', 'SalesController@getCustomer')->name('sales.getCustomer');
Route::get('sales/getstock/{id}', 'SalesController@getstock')->name('sales.getstock');
Route::get('sales/checkStockQty/{id}', 'SalesController@checkStockQty')->name('sales.checkStockQty');


Route::get('purchaseReport', 'ReportController@purchaseReport')->name('report.purchase');

Route::resource('producation', 'ProducationController');
Route::post('producation/store', 'ProducationController@store')->name('producation.store');
Route::get('producation/delete/{id}', 'ProducationController@destroy')->name('producation.delete');
Route::get('producation/getcommoditycategory/{id}', 'ProducationController@getcommoditycategory')->name('producation.getcommoditycategory');
Route::get('producation/getcurrentcommodity/{id}', 'ProducationController@getcurrentcommodity')->name('producation.getcurrentcommodity');

Route::resource('package', 'PackageController');
Route::post('package/store', 'PackageController@store')->name('package.store');
Route::get('package/delete/{id}', 'PackageController@destroy')->name('package.delete');
Route::get('package/getcommoditycategory/{id}', 'PackageController@getcommoditycategory')->name('package.getcommoditycategory');
Route::get('package/getcurrentcommodity/{id}', 'PackageController@getcurrentcommodity')->name('package.getcurrentcommodity');
