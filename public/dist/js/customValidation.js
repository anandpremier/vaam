// Commodity form Validation

$(document).ready(function() {

    $("#commodityFrom").validate({
        rules: {
            name: {
              required: true,
              maxlength: 50,
            }
        },
        messages: {
            name: {
                required: 'Please enter your name',
                maxlength: 'Name should not be more than 50 character',
                minlenght: 'Invalid Name ',
              },
        }
    });

});

// Commodity Category Validation

$(document).ready(function() {

    $("#commodityCatFrom").validate({
        rules: {
            name: {
                required: true,
                maxlength: 50,
              }
        },
        messages: {
            name: {
                required: 'Please enter your name',
                maxlength: 'Name should not be more than 50 character',
                minlenght: 'Invalid Name ',
              },
        }
    });

});


// Commodity Category Validation

$(document).ready(function() {

    $.validator.addMethod("ifsc", function(value, element) {
        var reg = /^[A-Za-z]{4}[0-9]{6,7}$/;
        if (this.optional(element)) {
            return true;
        }
        if (value.match(reg)) {
            return true;
        } else {
            return false;
        }
    }, "Please specify a valid IFSC CODE");

    $("#customerForm").validate({
        rules: {
            user_type_vendor: "required",
            name: "required",
            // email: {
            //     required: true,
            //     email: true
            // },
            mobile: {
                required: true,
                number: true
            },
            address: "required",
            //  account_holder: "required",
            // account_number: {
            //     required: true,
            //     minlength: 12,
            //     maxlength: 12,
            //     digits: true
            // },
            //bank_name: "required",
            // ifsc_code: {
            //     required: true,
            //     ifsc: true
            // }
        },
        messages: {
            company_name: "Please select your commodity",
            name: "Please enter your name",
            //   email: "Please enter your email",
            mobile: "Please enter your mobile number",
            address: "Please enter your address",
            //  account_holder: "Please enter your account holder name",
            // account_number: {
            //     digits: 'Account Number must be an number',
            //     minlength: 'Account number cannot be less than 12 digits',
            //     maxlength: 'Account number cannot be more than 12 digits',
            //     required: 'Please enter your account number'
            // },
            //bank_name: "Please enter your bank name",
            // ifsc_code: "Please enter your ifsc code"

        }
    });

});

// Expanse form Validation

$(document).ready(function() {

    $("#expanseForm").validate({
        rules: {
            user_id: "required",
            expanse_title: "required",
            amount: "required",
            date: "required"
        },
        messages: {
            user_id: "Please select user",
            expanse_title: "Please enter your expanse title",
            amount: "Please enter your amount",
            date: "Please select date"

        }
    });

});


// Expanse form Validation

$(document).ready(function() {

    $("#packageForm").validate({
        rules: {
            commodity_id: "required",
            commodity_category_id: "required",
            quantity: {
                lessThanEqual: "#currentQuantity"
            },
            date: "required",
            commodity_qty: "required",
            commodity_qty_size: "required",
            bag_size: "required",
        },
        messages: {
            commodity_id: "Please select commodity",
            quantity: "Please enter quantity",
            commodity_category_id: "Please select commodity quntity",
            date: "Please select date",

        }
    });

    $.validator.addMethod('lessThanEqual', function(value, element, param) {
        if (this.optional(element)) return true;
        var i = parseInt(value);
        var j = parseInt($(param).val());
        return i <= j;
    }, "The value {0} must be less than ");

});


// Expanse form Validation

$(document).ready(function() {

    $("#producationForm").validate({
        rules: {
            commodity_id: "required",
            commodity_category_id: "required",
            quantity: {
                required: true,
                lessThanEqual: "#currentQuantity"
            },
            date: "required"
        },
        messages: {
            commodity_id: "Please select commodity",
            commodity_category_id: "Please select commodity quntity",
            quantity: "Please enter valid Quntity",
            date: "Please select date"
        }
    });

});

// Vaam user form Validation

$(document).ready(function() {

    $("#vaamForm").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },
            mobile: {
                required: true,
                number: true
            }
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter your email",
            mobile: "Please enter your mobile"
        }
    });

});



// Vendor form Validation

$(document).ready(function() {

    $.validator.addMethod("ifsc", function(value, element) {
        var reg = /^[A-Za-z]{4}[0-9]{6,7}$/;
        if (this.optional(element)) {
            return true;
        }
        if (value.match(reg)) {
            return true;
        } else {
            return false;
        }
    }, "Please specify a valid IFSC CODE");

    $("#vendorForm").validate({
        rules: {
            user_type: "required",
            company_name: "required",
            name: "required",
            // email: {
            //     required: true,
            //     email: true
            // },
            mobile: {
                required: true,
                number: true
            },
            address: "required",
            //  account_holder: "required",
            // account_number: {
            //     required: true,
            //     minlength: 12,
            //     maxlength: 12,
            //     digits: true
            // },
            //bank_name: "required",
            // ifsc_code: {
            //     required: true,
            //     ifsc: true
            // }
        },
        messages: {
            user_type: "Please select user",
            company_name: "Please enter your company name",
            name: "Please enter your name",
            email: "Please enter your email",
            mobile: "Please enter your mobile",
            address: "Please enter your address",
            account_holder: "Please enter your account holder name",
            account_number: {
                digits: 'Account Number must be an number',
                minlength: 'Account number cannot be less than 12 digits',
                maxlength: 'Account number cannot be more than 12 digits',
                required: 'Please enter your account number'
            },
            bank_name: "Please enter your bank name",
            /// ifsc_code: "Please enter your ifsc code",
        }
    });

});

// Sales form Validation

$(document).ready(function() {

    $("#salesForm").validate({
        rules: {
            commodity_id: "required",
            commodity_category_id: "required",
             quantity: {
                 required: true,
                 lessThanEqual: "#select_qty_check"
             },
            rate: "required",
            net_amount: "required",
            commission: "required",
            transpotation: "required",
            labour: "required",
            gross_amount: "required",
            note: "required",
            sales_invoice_no: "required",
            type: "required",
            address: "required",
            mobile: "required",

        },
        messages: {
            commodity_id: "Please select commodity",
            commodity_category_id: "Please select commodity category",
            quantity: "Please enter your valid quantity",
            rate: "Please enter your rate",
            net_amount: "Please enter your net amount",
            commission: "Please enter your commission",
            transpotation: "Please enter your transpotation amount",
            labour: "Please enter your labour amount",
            gross_amount: "Please enter your gross amount",
            note: "Please enter your note",
            sales_invoice_no: "Please enter your sales invoice number",
            type: "Please enter your type",
            address: "Please enter your address",
            mobile: "Please enter your mobile number",
        }
    });

});


// Sales form Validation

$(document).ready(function() {

    $("#purchaseForm").validate({
        rules: {
            commodity_id: "required",
            commodity_category_id: "required",
            quantity: "required",
            rate: "required",
            net_amount: "required",
            commission: "required",
            transpotation: "required",
            labour: "required",
            gross_amount: "required",
            note: "required",
            purchase_invoice_no: "required",
            type: "required",
            address: "required",
            mobile: "required",

        },
        messages: {
            commodity_id: "Please select commodity",
            commodity_category_id: "Please select commodity category",
            quantity: "Please enter your quantity",
            rate: "Please enter your rate",
            net_amount: "Please enter your net amount",
            commission: "Please enter your commission",
            transpotation: "Please enter your transpotation amount",
            labour: "Please enter your labour amount",
            gross_amount: "Please enter your gross amount",
            note: "Please enter your note",
            purchase_invoice_no: "Please enter your purchase invoice number",
            type: "Please enter your type",
            address: "Please enter your address",
            mobile: "Please enter your mobile number",
        }
    });

});