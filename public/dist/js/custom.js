  $(function() {
      $('#vaamUsers').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
      });

  });

  $("#date").datepicker({
      startDate: new Date(),
      autoclose: true,
      pickDate: false,
      dateFormat: 'dd/mm/yy'
  });


  $(".form-check input[name='type']").click(function() {
      var checkRadio = $('input:radio[name=type]:checked').val();
       
      if (checkRadio == 1) {
          $('.customerClass').hide();
          $('.vendorClass').show();
      } else {
          $('.vendorClass').hide();
          $('.customerClass').show();

      }

  });

  //Purchase select commodity category
  $("#commodity_id").change(function() {
      var commodity_id = $('#commodity_id').val();
      $.ajax({
          type: 'get',
          url: '/purchase/getpurchasecommodity/' + commodity_id,
          success: function(response) {
              if (response.result == 'success') {
                  $('.purchaseRemove').remove();
                  $('.commodity_category_id option').remove();
                  $.each(response.html, function(key, value) {
                      $('#commodity_category_id').append('<option class="purchaseRemove" value="' + value['id'] + '">' + value['name'] + '</option>');
                  });
              }

          }
      });
  });


  // Producation commodity category select
  $("#producation_commodity_id").change(function() {

      var commodity_id = $('#producation_commodity_id').val();
      $.ajax({
          type: 'get',
          url: '/producation/getcommoditycategory/' + commodity_id,
          success: function(response) {
              if (response.result == 'success') {
                  $('.productionRemove').remove();
                  $('.currentQuantity').val();
                  $.each(response.html, function(key, value) {
                      $('#producation_commodity_category_id').append('<option class="productionRemove" value="' + value['id'] + '">' + value['name'] + '</option>');
                  });
              }

          }
      });
  });

  // Producation select commodity category select
  $("#commodity_category_id").change(function() {

      var commodity_id = $('#producation_commodity_id').val();
      var commodity_category_id = $('#commodity_category_id').val();
      var commodity_d = commodity_id + ',' + commodity_category_id;
      $.ajax({
          type: 'get',
          url: '/producation/getcurrentcommodity/' + commodity_d,
          success: function(response) {
              if (response.result == 'success') {
                  $('.currentQuantity').val();
                  // $('.productionRemove').remove();

                  $.each(response.html, function(key, value) {
                      $('#currentQuantity').val(value.purchase_stock);
                      $('#currentQuantity').val(value.production_stock);
                  });
              }
          }
      });
  });

  $(document).on('keyup change', '#rate, #net_amount,#commission,#transpotation,#labour', function() {

      var net_amount = parseFloat($('#net_amount').val());
      var commission = parseFloat($('#commission').val());
      var transpotation = parseFloat($('#transpotation').val());
      var labour = parseFloat($('#labour').val());
      var gross_amount = (net_amount + commission + transpotation + labour);
      $("#gross_amount").val(gross_amount);
  });

  $(document).on('keyup change', '#rate', function() {
      var quantity = parseFloat($('#quantity').val());
      var rate = parseFloat($('#rate').val());
      var net_amount = (quantity * rate);
      $("#net_amount").val(net_amount);
  });

  $("#vendor_id").change(function() {

      var vendor_id = $("#vendor_id").val();

      $.ajax({
          type: 'get',
          url: '/purchase/getpurchase/' + vendor_id,
          success: function(response) {

              if (response.result == 'success') {

                  $("#address").val(response.html['address']);
                  $("#mobile").val(response.html['mobile']);

              }

          }
      });
  });

  $("#customer_id").change(function() {

      var customer_id = $("#customer_id").val();

      if (customer_id) {
          $.ajax({
              type: 'get',
              url: '/purchase/getpurchasecustomer/' + customer_id,
              success: function(response) {

                  console.log(response.html);
                  if (response.result == 'success') {

                      $("#address").val(response.html['address']);
                      $("#mobile").val(response.html['mobile']);

                  }

              }
          });
      } else {
          $("#address").val();
          $("#mobile").val();
      }
  });

  $(".packageAdd").click(function() {

      commodity_qty = $('#commodity_qty').val();
      commodity_qty_size = $('#commodity_qty_size').val();
      bag_size = $('#bag_size').val();
      quantity = $('#quantity').val();

      $('#table').append('<tr><td>' + commodity_qty + '</td><td>' + commodity_qty_size + '</td><td>' + bag_size + '</td><td>' + quantity + '</td></tr>');
  });


  //Package select commodity category
  $("#package_commodity_id").change(function() {
      var commodity_id = $('#package_commodity_id').val();
      $.ajax({
          type: 'get',
          url: '/purchase/getpurchasecommodity/' + commodity_id,
          success: function(response) {
              if (response.result == 'success') {
                  $('.purchaseRemove').remove();
                  $('#currentQuantity').val('');
                  $.each(response.html, function(key, value) {
                      $('#commodity_category_id').append('<option class="purchaseRemove" value="' + value['id'] + '">' + value['name'] + '</option>');
                      $('#commodity_qty').append('<option class="purchaseRemove" value="' + value['id'] + '">' + value['name'] + '</option>');
                  });
              }

          }
      });
  });

  //Bag calculation

  $("#bag_size").blur(function() {

      var bagQtySize = $('#commodity_qty_size').val();
      var bagSize = $('#bag_size').val();

      var converter = bagQtySize / 1000 * bagSize;
      $("#quantity").val(converter);
      $("#packageData").removeClass("isDisabled");
  });

  //Purchase vendor select
  $("#purchase_vendor_id").click(function() {
      var commodity_id = $('#purchase_vendor_id').val();

      $.ajax({
          type: 'get',
          url: '/purchase/getpurchaseVendor/' + commodity_id,
          success: function(response) {
              if (response.result == 'success') {
              
                  $('.purchaseRemove1').remove();
                  $('#vendor_user_id option').remove();
                 
                  $.each(response.html, function(key, value) {
                      $('#vendor_user_id').append('<option class="purchaseRemove1" value="' + value['id'] + '">' + value['name'] + '</option>');

                  });
              }

          }
      });
  });

  //Purchase vendor user
  $("#vendor_user_id").click(function() {

      var vendor_id = $("#vendor_user_id").val();
       $.ajax({
          type: 'get',
          url: '/purchase/getpurchase/' + vendor_id,
          success: function(response) {
              
              if (response.result == 'success') {

                  $("#address").val(response.html['address']);
                  $("#mobile").val(response.html['mobile']);

              }

          }
      });
  });

  // package select commodity category select
  $("#producation_commodity_category_id").change(function() {

      var commodity_id = $('#producation_commodity_id').val();
      var commodity_category_id = $('#producation_commodity_category_id').val();
      var commodity_d = commodity_id + ',' + commodity_category_id;

      $.ajax({
          type: 'get',
          url: '/package/getcurrentcommodity/' + commodity_d,
          success: function(response) {
              if (response.result == 'success') {
                  $('#currentQuantity').val('');
                  $.each(response.html, function(key, value) {
                      $('#currentQuantity').val(value.purchase_stock);
                  });
              }
          }
      });
  });

  // package select commodity category select
  $("#commodity_category_id").change(function() {

      var commodity_id = $('#package_commodity_id').val();
      var commodity_category_id = $('#commodity_category_id').val();
      var commodity_d = commodity_id + ',' + commodity_category_id;

      $.ajax({
          type: 'get',
          url: '/package/getcurrentcommodity/' + commodity_d,
          success: function(response) {
              if (response.result == 'success') {
                  $('#currentQuantity').val('');

                  $.each(response.html, function(key, value) {

                      $('#currentQuantity').val(value.production_stock);
                  });
              }

          }
      });
  });

  $("#user_type_vendor").change(function() {
      var user_type_vendor = $('#user_type_vendor').val();

      if (user_type_vendor == 1) {
          $('.showVendorType').show();

      } else {
          $('.showVendorType').hide();
      }

  });

  // Package data
  jQuery("#packageData").click(function() {

      $('.tabelAdd').show();
      var commodity_qty = $('#commodity_qty').val();
      var conceptName = $('#commodity_qty').find(":selected").text();
     
      var commodity_qty_size = $('#commodity_qty_size').val();
      var bag_size = $('#bag_size').val();
      var quantity = $('#quantity').val();

      var package = [commodity_qty, commodity_qty_size, bag_size, quantity];

      var markup = "<tr><td><input readonly type='hidden' name='package_commodity_qty[]' value=" + commodity_qty + "><input readonly type='text' value=" + conceptName + "></td><td><input readonly type='text' name='package_commodity_qty_size[]' value=" + commodity_qty_size + "></td><td><input readonly type='text' name='package_bag_size[]' value=" + bag_size + "></td><td><input readonly type='text' name='package_quantity[]' value=" + quantity + " </td></tr> ";
      $("table tbody").append(markup);

      currentQuantity = $('#currentQuantity').val();
      var updateCurrentQuantity = currentQuantity - quantity;
      if (updateCurrentQuantity == 0) {
          $("#packageData").addClass("isDisabled");
      }
      $('#currentQuantity').val(updateCurrentQuantity);
      $('#quantity').val('');

  });


  // sales select commodity category select
  $(".sales_commodity_category_id").change(function() {

      var commodity_id = $('.sale_commodity_id').val();
      var commodity_category_id = $('.sales_commodity_category_id').val();
      var commodity_d = commodity_id + ',' + commodity_category_id;

      $.ajax({
          type: 'get',
          url: '/producation/getcurrentcommodity/' + commodity_d,
          success: function(response) {
              if (response.result == 'success') {
                  $('.currentQuantity').val();
                  // $('.productionRemove').remove();

                  $.each(response.html, function(key, value) {
                      $('#currentQuantity').val(value.packed_stock);
                  });
              }

          }
      });
  });

  //Purchase vendor select
  $("#sale_customer_id").change(function() {
      var commodity_id = $('#sale_customer_id').val();

      $.ajax({
          type: 'get',
          url: '/sales/getCustomer/' + commodity_id,
          success: function(response) {
              if (response.result == 'success') {
                  $('.purchaseRemove1').remove();
                  $.each(response.html, function(key, value) {
                      $('#vendor_user_id').append('<option class="purchaseRemove1" value="' + value['id'] + '">' + value['name'] + '</option>');

                  });
              }

          }
      });
  });

   // Producation select commodity category select
   $(".sales_select_commodity_category_id").change(function() {
    var commodity_id = $('#commodity_id').val();
    var commodity_category_id = $('.sales_select_commodity_category_id').val();
    var commodity_d = commodity_id + ',' + commodity_category_id;
      
    $.ajax({
        type: 'get',
        url: '/sales/getstock/' + commodity_d,
        success: function(response) {
            if (response.result == 'success') {
               // $('.select_quantity').remove();
                $.each(response.html, function(key, value) {
                
                    if(response.checkBag[key] == 0){
                        var bag = value['bag'];
                    }else{
                        var bag = value['bag'] - response.checkBag[key];
                    }
                   
                    var bagSize = value['quantity'] / value['bag'];
                   // ('#currentQuantity').val(value['quantity']);
                    $('#select_quantity').append('<option class="select_quantity" value="' + value['id'] + '">' + 'quantity ' + value['quantity'] + ', Bag '+ bag +', Bag size ' +bagSize+'KG',  '</option>');
                });
            }

        }
    });
});

$("#select_quantity").change(function() {
    var select_quantity = $('#select_quantity').val();
    $.ajax({
        type: 'get',
        url: '/sales/checkStockQty/' + select_quantity,
        success: function(response) {
            if (response.result == 'success') {
               // $('.select_quantity').remove();
            
                 $('#select_qty_check').val(response.html);
            }

        }
    });
  
});