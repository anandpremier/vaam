<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{

    protected $table = 'commodity_mgmt';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = true;

    public function Commodity_category(){

        return $this->hasMany('App\Models\Commodity_category');
    }
}
