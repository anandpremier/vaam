<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Expanse extends Model
{

    protected $table = 'expanse';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = false;
}
