<?php

namespace App\Models;
use App\Models\StockMgmt;
use Illuminate\Database\Eloquent\Model;

class Producation extends Model
{
    protected $table = 'producations';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = true;


    public function stockMgmts(){

        return $this->hasOne('App\Models\StockMgmt', 'producation_id', 'id');
    }
}
