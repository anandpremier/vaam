<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockMgmt extends Model
{

    protected $table = 'stock_mgmts';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = true;
}
