<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commodity_category extends Model
{
    protected $table = 'commodity_categories';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = true;
}
