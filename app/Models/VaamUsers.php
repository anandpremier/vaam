<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class VaamUsers extends Model
{

    protected $table = 'vaamUsers';
    //protected $connection = 'vaamUsers';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public $timestamps = true;
}
