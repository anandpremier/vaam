<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{

    protected $table = 'sales';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = true;

    public function stockMgmts(){

        return $this->hasOne('App\Models\StockMgmt');
    }
}
