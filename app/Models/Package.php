<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = true;


    public function stockMgmts(){

        return $this->hasOne('App\Models\StockMgmt', 'producation_id', 'id');
    }
}
