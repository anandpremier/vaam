<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{

    protected $table = 'purchase';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $timestamps = true;
}
