<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateCommodityCategoryRequest;
use App\Http\Requests\CommodityCategoryStoreRequest;
use App\Models\Commodity;
use App\Models\Commodity_category;
use Exception;
use Illuminate\Support\Facades\Log;

class CommoditycategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commodityCategory = Commodity_category::paginate(15);
        return view('commodityCategory.index',['commodityCategory' => $commodityCategory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $commodity = $id;
        return view('commodityCategory.create',compact('commodity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommodityCategoryStoreRequest $request)
    {
        try {
            $validated = $request->validated();
            if(!empty($validated)){
              $users = new Commodity_category();

              $data['commodity_id'] = $request->commodity_id;
              $data['name'] = $request->name;
              $data['created_at'] = date('Y-m-d H:i:s');
              $data['updated_at'] = date('Y-m-d H:i:s');
              $users->insert($data);
              Log::info('Sussfully added');
              toastr()->success('Data has been saved successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('commodity');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $commodity = Commodity::get();
        $user = Commodity_category::find($id);
        return view('commodityCategory.edit',['user' => $user,'commodity' => $commodity]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommodityCategoryRequest $request, $id)
    {

        try {
            $validated = $request->validated();
            if(!empty($validated)){

              $data['commodity_id'] = $request->commodity_id;
              $data['name'] = $request->name;
              $data['updated_at'] = date('Y-m-d H:i:s');
              Commodity_category::where('id',$id)->update($data);
              Log::info('successfully updated ');
              toastr()->success('Data has been updated successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('commodity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(!empty($id)){
                $deleteUser = Commodity_category::find($id);
                $deleteUser->delete();
                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            }else{
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('commodity');
     }
}
