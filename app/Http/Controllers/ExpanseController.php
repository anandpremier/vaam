<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ExpanseStoreRequest;
use App\Http\Requests\UpdateExpanseStoreRequest;
use App\Models\Expanse;
use App\Models\VaamUsers;

use Exception;
use Illuminate\Support\Facades\Log;

class ExpanseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Expanse::paginate(15);
        return view('expanse.index',['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vaamUser = VaamUsers::get();
        return view('expanse.create',compact('vaamUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpanseStoreRequest $request)
    {

        try {
            $validated = $request->validated();

            if(!empty($validated)){
              $users = new Expanse();

              $data['user_id'] = $request->user_id;

              $data['expanse_title'] = $request->expanse_title;

              $data['amount'] = $request->amount;

              $data['date'] = $request->date;
              $data['created_at'] = date('Y-m-d H:i:s');
              $data['updated_at'] = date('Y-m-d H:i:s');

              $users->insert($data);
              Log::info('Sussfully added');
              toastr()->success('Data has been saved successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('expanse');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = Expanse::find($id);
        $vaamUser = VaamUsers::get();
        return view('expanse.edit',['user' => $user,'vaamUser'=>$vaamUser]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExpanseStoreRequest $request, $id)
    {

        try {
            $validated = $request->validated();
            if(!empty($validated)){

                $data['user_id'] = $request->user_id;
                $data['expanse_title'] = $request->expanse_title;
                $data['amount'] = $request->amount;
                $data['date'] = $request->date;
                $data['updated_at'] = date('Y-m-d H:i:s');

             Expanse::where('id',$id)->update($data);
              Log::info('successfully updated ');
              toastr()->success('Data has been updated successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('expanse');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(!empty($id)){
                $deleteUser = Expanse::find($id);
                $deleteUser->delete();
                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            }else{
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('expanse');
    }
}
