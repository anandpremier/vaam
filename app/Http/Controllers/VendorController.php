<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VendorStoreRequest;
use App\Http\Requests\UpdateVendorStoreRequest;
use App\Models\Vendor;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Vendor::paginate(15);
        return view('vendor.index',['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userType = Config::get('constants.userType');
        return view('vendor.create',compact('userType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorStoreRequest $request)
    {
        try {
            $validated = $request->validated();
            if(!empty($validated)){
              $users = new Vendor();

              $data['user_type'] = $request->user_type;
              $data['company_name'] = $request->company_name;
              $data['name'] = $request->name;
              $data['email'] = $request->email;
              $data['mobile'] = $request->mobile;
              $data['address'] = $request->address;
              $data['account_holder'] = $request->account_holder;
              $data['account_number'] = $request->account_number;
              $data['bank_name'] = $request->bank_name;
              $data['ifsc_code'] = $request->ifsc_code;
              $data['created_at'] = date('Y-m-d H:i:s');
              $data['updated_at'] = date('Y-m-d H:i:s');
              $users->insert($data);
              Log::info('Sussfully added');
              toastr()->success('Data has been saved successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('vendor');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Vendor::find($id);
        $userType = Config::get('constants.userType');
        return view('vendor.edit',['user' => $user,'userType'=> $userType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVendorStoreRequest $request, $id)
    {

        try {
            $validated = $request->validated();
            if(!empty($validated)){

                $data['user_type'] = $request->user_type;
                if($request->user_type == 2){
                    $userType = null;
                }else{
                    $userType = $request->company_name;
                }
                $data['company_name'] = $userType;
                $data['name'] = $request->name;
                $data['email'] = $request->email;
                $data['mobile'] = $request->mobile;
                $data['address'] = $request->address;
                $data['account_holder'] = $request->account_holder;
                $data['account_number'] = $request->account_number;
                $data['bank_name'] = $request->bank_name;
                $data['ifsc_code'] = $request->ifsc_code;
                $data['updated_at'] = date('Y-m-d H:i:s');
              Vendor::where('id',$id)->update($data);
              Log::info('successfully updated ');
              toastr()->success('Data has been updated successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('vendor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(!empty($id)){
                $deleteUser = Vendor::find($id);
                $deleteUser->delete();

                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            }else{
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('vendor');
    }
}
