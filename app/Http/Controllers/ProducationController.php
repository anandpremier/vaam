<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use App\Models\Commodity_category;
use App\Http\Requests\ProducationStoreRequest;
use App\Http\Requests\UpdateProducationStoreRequest;
use App\Models\Producation;
use App\Models\StockMgmt;
use Illuminate\Http\Request;
use Exception;

use Illuminate\Support\Facades\Log;

class ProducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recodes = Producation::paginate(15);
        return view('producation.index',['recodes' => $recodes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commodity = Commodity::get();
        //$commodityCategory = Commodity_category::where('name','raw')->get();
        return view('producation.create',compact('commodity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProducationStoreRequest $request)
    {

        try {
            $validated = $request->validated();

            if(!empty($validated)){
                $users = new Producation();

                        $data['commodity_id'] = $request->commodity_id;
                        $data['commodity_category_id'] = $request->commodity_category_id;
                        $data['date'] = $request->date;
                        $data['quantity'] = $request->quantity;

                        $data['created_at'] = date('Y-m-d H:i:s');
                        $data['updated_at'] = date('Y-m-d H:i:s');

                        $users->insert($data);

                        //Stock Managegment

                        $checkValue = StockMgmt::where('commodity_id',$request->commodity_id)
                        ->where('commodity_category_id',$request->commodity_category_id)
                        ->first();

                        if($checkValue->production_stock == 0){
                            $producation_stock = $request->quantity;
                        }else{
                            $producation_stock = $checkValue->production_stock + $request->quantity;
                        }
                        $currentPurchaseStock = $checkValue->purchase_stock - $request->quantity;
                            if(isset($checkValue)){
                            $stockData['purchase_stock'] = $currentPurchaseStock;
                            $stockData['production_stock'] = $producation_stock;
                            $stockData['updated_at'] = date('Y-m-d H:i:s');
                            StockMgmt::where('id',$checkValue->id)->update($stockData);
                            }


                        Log::info('Sussfully added');
                        toastr()->success('Data has been saved successfully!');



            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('producation');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producation  $producation
     * @return \Illuminate\Http\Response
     */
    public function show(Producation $producation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producation  $producation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producation  $producation
     * @return \Illuminate\Http\Response
     */
    public function update($request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producation  $producation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(!empty($id)){
                $deleteUser = Producation::find($id);

                $deleteStock = StockMgmt::where('commodity_id',$deleteUser->commodity_id)
                ->where('commodity_category_id',$deleteUser->commodity_category_id)
                ->first();

                $finalProductionStock = $deleteStock->production_stock - $deleteUser->quantity;
                $finalpurchesStock = $deleteStock->purchase_stock + $deleteUser->quantity;

                $stockData['purchase_stock'] = $finalpurchesStock;
                $stockData['production_stock'] = $finalProductionStock;
                $stockData['updated_at'] = date('Y-m-d H:i:s');
                StockMgmt::where('id',$deleteStock->id)->update($stockData);

                $deleteUser->delete();
                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            }else{
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('producation');
    }
    public function getcommoditycategory($id){

        $commodity = Commodity::where('id',$id)->first();
        $commodityCategory = Commodity_category::where('commodity_id',$commodity->id)
        // ->where(function($query) {
        //     $query->where('name', 'raw');
        // })
        ->get();
        return response()->json(["result" => "success", "status" => 200, 'html' => $commodityCategory]);


    }

    public function getcurrentcommodity($id){


        $str_arr = preg_split ("/\,/", $id);

        $commodity_id = $str_arr[0];
        $commodity_category_id = $str_arr[1];

        $commodityCategory = StockMgmt::where('commodity_id',$commodity_id)
        ->where('commodity_category_id', $commodity_category_id)
        ->get();

        return response()->json(["result" => "success", "status" => 200, 'html' => $commodityCategory]);


    }
}
