<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommodityStoreRequest;
use App\Http\Requests\UpdateCommodityRequest;
use App\Models\Commodity;
use App\Models\Commodity_category;
use Exception;
use Illuminate\Support\Facades\Log;

class CommodityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Commodity::paginate(15);
        return view('commodity.index',['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('commodity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommodityStoreRequest $request)
    {
        try {
            $validated = $request->validated();
            if(!empty($validated)){
              $users = new Commodity();

              $data['name'] = $request->name;
              $data['created_at'] = date('Y-m-d H:i:s');
              $data['updated_at'] = date('Y-m-d H:i:s');
              $users->insert($data);

              $commodityCategory = new Commodity_category();

              $categoryData['commodity_id'] = Commodity::max('id');
              $categoryData['name'] = 'raw';
              $categoryData['created_at'] = date('Y-m-d H:i:s');
              $categoryData['updated_at'] = date('Y-m-d H:i:s');
              $commodityCategory->insert($categoryData);

              Log::info('Sussfully added');
              toastr()->success('Data has been saved successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('commodity');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $commodityQty = Commodity_category::where('commodity_id',$id)->get();
        $commodityQtyPaginate = Commodity_category::paginate(15);
        $user = Commodity::find($id);

        return view('commodity.edit',['user' => $user,'commodityQtys' => $commodityQty,'commodityQtyPaginate' => $commodityQtyPaginate]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommodityRequest $request, $id)
    {

        try {
            $validated = $request->validated();
            if(!empty($validated)){

              $data['name'] = $request->name;
              $data['updated_at'] = date('Y-m-d H:i:s');
              $users = Commodity::paginate(1);
              Commodity::where('id',$id)->update($data);
              Log::info('successfully updated ');
              toastr()->success('Data has been updated successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('commodity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(!empty($id)){

                $cat = Commodity_category::where('commodity_id',$id)->get();
              
                foreach($cat as $value){
                  $Commodity_category = Commodity_category::find($value->id);
                  $Commodity_category->delete();
                }
                $deleteUser = Commodity::find($id);
                $deleteUser->delete();

                Log::info('successfully deleted');
                toastr()->success('Data has been deleted successfully!');
            }else{
            
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
          
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('commodity');
     }
}
