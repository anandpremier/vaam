<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerStoreRequest;
use App\Http\Requests\UpdateCustomerStoreRequest;
use App\Models\Customer;
use Exception;
use Illuminate\Support\Facades\Log;
use Config;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Customer::paginate(15);
        return view('customer.index',['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userType = Config::get('constants.userType');
        return view('customer.create',compact('userType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerStoreRequest $request)
    {

        try {
            $validated = $request->validated();
            if(!empty($validated)){
              $users = new Customer();

              $data['user_type'] = $request->user_type;
              $data['company_name'] = $request->company_name;
              $data['name'] = $request->name;
              $data['email'] = $request->email;
              $data['mobile'] = $request->mobile;
              $data['address'] = $request->address;
              $data['account_holder'] = $request->account_holder;
              $data['account_number'] = $request->account_number;
              $data['bank_name'] = $request->bank_name;
              $data['ifsc_code'] = $request->ifsc_code;
              $data['created_at'] = date('Y-m-d H:i:s');
              $data['updated_at'] = date('Y-m-d H:i:s');
              $users->insert($data);
              Log::info('Sussfully added');
              toastr()->success('Data has been saved successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('customer');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Customer::find($id);
        $userType = Config::get('constants.userType');
        return view('customer.edit',['user' => $user,'userType'=> $userType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerStoreRequest $request, $id)
    {

        try {
            $validated = $request->validated();
            if(!empty($validated)){

                $data['user_type'] = $request->user_type;

                if($request->user_type == 2){
                    $data['company_name'] = null;
                }else{
                    $data['company_name'] = $request->company_name;
                }

                $data['name'] = $request->name;
                $data['email'] = $request->email;
                $data['mobile'] = $request->mobile;
                $data['address'] = $request->address;
                $data['account_holder'] = $request->account_holder;
                $data['account_number'] = $request->account_number;
                $data['bank_name'] = $request->bank_name;
                $data['ifsc_code'] = $request->ifsc_code;
                $data['updated_at'] = date('Y-m-d H:i:s');
              Customer::where('id',$id)->update($data);
              Log::info('successfully updated ');
              toastr()->success('Data has been updated successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(!empty($id)){
                $deleteUser = Customer::find($id);
                $deleteUser->delete();
                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            }else{
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('customer');
    }
}
