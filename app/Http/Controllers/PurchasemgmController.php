<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PurchaseStoreRequest;
use App\Http\Requests\UpdatePurchaseStoreRequest;
use App\Models\Commodity;
use App\Models\Commodity_category;
use App\Models\Customer;
use App\Models\Expanse;
use App\Models\VaamUsers;
use App\Models\Vendor;
use App\Models\Purchase;
use App\Models\StockMgmt;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PurchasemgmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Purchase::paginate(15);
        return view('purchase.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commodity = Commodity::get();
        $vendor = Vendor::whereNotNull('company_name')
            ->get()
            ->unique('company_name');
        $customer = Vendor::whereNull('company_name')
            ->get();

        return view('purchase.create', compact('commodity', 'vendor', 'customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseStoreRequest $request)
    {

        try {
            $validated = $request->validated();
            //  dd($validated);
            if (!empty($validated)) {

                $users = new Purchase();
                $data['commodity_id'] = $request->commodity_id;
                $data['commodity_category_id'] = $request->commodity_category_id;
                $data['quantity'] = $request->quantity;
                $data['rate'] = $request->rate;
                $data['commission'] = $request->commission;
                $data['transpotation'] = $request->transpotation;
                $data['labour'] = $request->labour;
                $data['net_amount'] = $request->net_amount;
                $data['gross_amount'] = $request->gross_amount;

                if ($request->note != null) {
                    $data['note'] = $request->note;
                }

                if ($request->company_vendor_id != null) {
                    $data['company_vendor_id'] = $request->company_vendor_id;
                }

                $data['purchase_invoice_no'] = $request->purchase_invoice_no;
                $data['type'] = $request->type;
                $data['vendor_id'] = $request->vendor_id;
                $data['address'] = $request->address;
                $data['mobile'] = $request->mobile;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                //dd($data);
                $users->insert($data);

                //Stock Managegment

                $stockCheck = StockMgmt::where('commodity_id', $request->commodity_id)
                    ->where('commodity_category_id', $request->commodity_category_id)
                    ->first();

                if ($stockCheck) {

                    $stockData['commodity_id'] = $request->commodity_id;
                    $stockData['commodity_category_id'] = $request->commodity_category_id;
                    $stockData['purchase_stock'] = $stockCheck->purchase_stock + $request->quantity;
                    $stockData['created_at'] = date('Y-m-d H:i:s');
                    $stockData['updated_at'] = date('Y-m-d H:i:s');
                    StockMgmt::where('id', $stockCheck->id)->update($stockData);
                } else {
                    $stockMgmt = new StockMgmt();
                    $stockData['commodity_id'] = $request->commodity_id;
                    $stockData['commodity_category_id'] = $request->commodity_category_id;
                    $stockData['purchase_stock'] = $request->quantity;
                    $stockData['created_at'] = date('Y-m-d H:i:s');
                    $stockData['updated_at'] = date('Y-m-d H:i:s');
                    $stockMgmt->insert($stockData);
                }


                Log::info('Sussfully added');
                toastr()->success('Data has been saved successfully!');
            } else {
                throw new Exception("Something went wrong please try again");
            }
        }

        //catch exception
        catch (Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' . $e->getMessage();
        }
        return redirect('purchase');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $commodity = Commodity::get();

        $user = Purchase::find($id);
        $commodityCategory = Commodity_category::where('commodity_id', $user->commodity_id)
            ->get();

        $getvendorInfo = Vendor::where('id', $user->vendor_id)->first();
        if ($getvendorInfo->company_name != null) {
            $vendorCompany = Vendor::where('company_name', $getvendorInfo->company_name)->get();
        } else {
            $vendorCompany = Vendor::whereNull('company_name')->get();
        }

        $vendor = Vendor::whereNotNull('company_name')
            ->get()
            ->unique('company_name');

        $customer = Vendor::whereNull('company_name')
            ->get();

        //$customer = Customer::get();

        return view('purchase.edit', ['vendorCompany' => $vendorCompany, 'user' => $user, 'commodity' => $commodity, 'vendor' => $vendor, 'customer' => $customer, 'commodityCategory' => $commodityCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePurchaseStoreRequest $request, $id)
    {

        try {
            $validated = $request->validated();
            if (!empty($validated)) {

                $data['commodity_id'] = $request->commodity_id;
                $data['commodity_category_id'] = $request->commodity_category_id;
                $data['quantity'] = $request->quantity;
                $data['rate'] = $request->rate;
                $data['commission'] = $request->commission;
                $data['transpotation'] = $request->transpotation;
                $data['labour'] = $request->labour;
                $data['net_amount'] = $request->net_amount;
                $data['gross_amount'] = $request->gross_amount;

                if ($request->note != null) {
                    $data['note'] = $request->note;
                }

                $data['purchase_invoice_no'] = $request->purchase_invoice_no;
                $data['type'] = $request->type;
                $data['vendor_id'] = $request->vendor_id;
                $data['address'] = $request->address;
                $data['mobile'] = $request->mobile;
                $data['updated_at'] = date('Y-m-d H:i:s');
                //dd($data);
                Purchase::where('id', $id)->update($data);

                $checkValue = StockMgmt::where('commodity_id', $request->commodity_id)
                    ->where('commodity_category_id', $request->commodity_category_id)
                    ->first();

                if (isset($checkValue)) {
                    $stockData['commodity_id'] = $request->commodity_id;
                    $stockData['commodity_category_id'] = $request->commodity_category_id;
                    $stockData['purchase_stock'] = $request->quantity;
                    $stockData['updated_at'] = date('Y-m-d H:i:s');
                    StockMgmt::where('id', $checkValue->id)->update($stockData);
                }

                Log::info('successfully updated');
                toastr()->success('Data has been updated successfully!');
            } else {
                throw new Exception("Something went wrong please try again");
            }
        }

        //catch exception
        catch (Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' . $e->getMessage();
        }
        return redirect('purchase');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (!empty($id)) {
                $deleteUser = Purchase::find($id);
                $deleteUser->delete();
                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            } else {
                toastr()->error('Something went wrong please try again');
            }
        }

        //catch exception
        catch (Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' . $e->getMessage();
        }
        return redirect('purchase');
    }

    public function getpurchase($id)
    {
        $vendro = Vendor::where('id', $id)->first();
        return response()->json(["result" => "success", "status" => 200, 'html' => $vendro]);
    }

    public function getpurchaseVendor($id)
    {
        $vendroData = Vendor::where('id', $id)->first();
        $vendro = Vendor::where('company_name', $vendroData->company_name)->get();
        return response()->json(["result" => "success", "status" => 200, 'html' => $vendro]);
    }

    public function getpurchasecustomer($id)
    {
        $vendro = Vendor::where('id', $id)->first();
        return response()->json(["result" => "success", "status" => 200, 'html' => $vendro]);
    }

    public function getpurchasecommodity($id)
    {

        $commodity = Commodity::where('id', $id)->first();
        $commodityCategory = Commodity_category::where('commodity_id', $commodity->id)->get();
        return response()->json(["result" => "success", "status" => 200, 'html' => $commodityCategory]);
    }
    public function getcomapnyUser($id)
    {


        $vendor = Vendor::find($id);
        $vendorUser = Vendor::where('company_name', $vendor->company_name)->get();

        return response()->json(["result" => "success", "status" => 200, 'html' => $vendorUser]);
    }
}
