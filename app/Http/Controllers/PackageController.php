<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\Commodity;
use App\Models\Commodity_category;
use App\Http\Requests\PackageStoreRequest;
use App\Models\StockMgmt;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Config;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $recodes = Package::paginate(15);
        return view('package.index', ['recodes' => $recodes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commodity = Commodity::get();
        //$commodityCategory = Commodity_category::where('name','raw')->get();
        return view('package.create', compact('commodity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            // $validated = $request->validated();

            // if(!empty($validated)){


            foreach ($request->package_commodity_qty as $key => $value) {

                $users = new Package();
                $data['commodity_id'] = $request->commodity_id;
                $data['commodity_category_id'] = $request->commodity_category_id;
                $data['date'] = $request->date;
                $data['commodity_qty_type'] = $value;
                $data['bag_size'] = $request->package_commodity_qty_size[$key];
                $data['bag'] = $request->package_bag_size[$key];
                $data['quantity'] = $request->package_quantity[$key];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $users->insert($data);
            }

            $checkValue = StockMgmt::where('commodity_id', $request->commodity_id)
                ->where('commodity_category_id', $request->commodity_category_id)
                ->first();

            $checkTotalVal = Package::where('commodity_id', $request->commodity_id)
                ->where('commodity_category_id', $request->commodity_category_id)
                ->sum('quantity');

            if ($checkValue->packed_stock == 0) {
                $packed_stock = $checkTotalVal;
            } else {
                $packed_stock = $checkValue->packed_stock + $checkTotalVal;
            }
            $currentproductionStock = $checkValue->production_stock - $checkTotalVal;
            if (isset($checkValue)) {
                $stockData['packed_stock'] = $packed_stock;
                $stockData['production_stock'] = $currentproductionStock;
                $stockData['updated_at'] = date('Y-m-d H:i:s');
                StockMgmt::where('id', $checkValue->id)->update($stockData);
            }
            Log::info('Sussfully added');
            toastr()->success('Data has been saved successfully!');
        }

        //catch exception
        catch (Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' . $e->getMessage();
        }
        return redirect('package');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (!empty($id)) {
                $deleteUser = Package::find($id);

                $deleteStock = StockMgmt::where('commodity_id', $deleteUser->commodity_id)
                    ->where('commodity_category_id', $deleteUser->commodity_category_id)
                    ->first();

                $finalPackageStock = $deleteStock->packed_stock - $deleteUser->quantity;
                $finalProducationStock = $deleteStock->production_stock + $deleteUser->quantity;

                $stockData['packed_stock'] = $finalPackageStock;
                $stockData['production_stock'] = $finalProducationStock;
                $stockData['updated_at'] = date('Y-m-d H:i:s');
                StockMgmt::where('id', $deleteStock->id)->update($stockData);

                $deleteUser->delete();
                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            } else {
                toastr()->error('Something went wrong please try again');
            }
        }

        //catch exception
        catch (Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' . $e->getMessage();
        }
        return redirect('package');
    }

    public function getcommoditycategory($id)
    {

        $commodity = Commodity::where('id', $id)->first();
        $commodityCategory = Commodity_category::where('commodity_id', $commodity->id)
            // ->where(function($query) {
            //     $query->where('name', 'raw');
            // })
            ->get();
        return response()->json(["result" => "success", "status" => 200, 'html' => $commodityCategory]);
    }

    public function getcurrentcommodity($id)
    {

        $str_arr = preg_split("/\,/", $id);
        $commodity_id = $str_arr[0];
        $commodity_category_id = $str_arr[1];
        $commodityCategory = StockMgmt::where('commodity_id', $commodity_id)
            ->where('commodity_category_id', $commodity_category_id)
            ->get();

        return response()->json(["result" => "success", "status" => 200, 'html' => $commodityCategory]);
    }
}
