<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VaamuserStoreRequest;
use App\Http\Requests\UpdateVaamuserStoreRequest;
use App\Models\VaamUsers;
use Exception;
use Illuminate\Support\Facades\Log;

class VaamUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = VaamUsers::paginate(15);
        return view('vaamUser.index',['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vaamUser.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VaamuserStoreRequest $request)
    {
        try {
            $validated = $request->validated();
            if(!empty($validated)){
              $users = new VaamUsers();

              $data['name'] = $request->name;
              $data['email'] = $request->email;
              $data['mobile'] = $request->mobile;
              $data['created_at'] = date('Y-m-d H:i:s');
              $data['updated_at'] = date('Y-m-d H:i:s');
              $users->insert($data);
              Log::info('Sussfully added');
              toastr()->success('Data has been saved successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('vaamUsers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = VaamUsers::find($id);
        return view('vaamUser.edit',['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVaamuserStoreRequest $request, $id)
    {

        try {
            $validated = $request->validated();
            if(!empty($validated)){

              $data['name'] = $request->name;
              $data['email'] = $request->email;
              $data['mobile'] = $request->mobile;
              $data['updated_at'] = date('Y-m-d H:i:s');
              $users = VaamUsers::paginate(1);
              VaamUsers::where('id',$id)->update($data);
              Log::info('successfully updated ');
              toastr()->success('Data has been updated successfully!');

            }else{
                throw new Exception("Something went wrong please try again");
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('vaamUsers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(!empty($id)){
                $deleteUser = VaamUsers::find($id);
                $deleteUser->delete();
                Log::info('successfully deleted ');
                toastr()->success('Data has been deleted successfully!');
            }else{
                toastr()->error('Something went wrong please try again');
            }

          }

          //catch exception
          catch(Exception $e) {
            Log::error($e->getMessage());
            toastr()->error('Something went wrong please try again');
            echo 'Message: ' .$e->getMessage();
          }
          return redirect('vaamUsers');
    }
}
