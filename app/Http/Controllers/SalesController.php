<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SalesStoreRequest;
use App\Http\Requests\UpdateSalesStoreRequest;
use App\Models\Commodity;
use App\Models\Customer;
use App\Models\Package;
use App\Models\Vendor;
use App\Models\Sales;
use App\Models\StockMgmt;
use Exception;
use Illuminate\Support\Facades\Log;

class SalesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = Sales::paginate(15);
    return view('sales.index', ['users' => $users]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $commodity = Commodity::get();
    $vendor = Customer::whereNotNull('company_name')
      ->get()
      ->unique('company_name');
    $customer = Customer::whereNull('company_name')
      ->get();

    return view('sales.create', compact('commodity', 'vendor', 'customer'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(SalesStoreRequest $request)
  {

    try {
      $validated = $request->validated();

      if (!empty($validated)) {
        $users = new Sales();

        $data['commodity_id'] = $request->commodity_id;
        $data['commodity_category_id'] = $request->commodity_category_id;
        $data['quantity'] = $request->quantity;
        $data['select_quantity'] = $request->select_quantity;
        $data['rate'] = $request->rate;
        $data['commission'] = $request->commission;
        $data['transpotation'] = $request->transpotation;
        $data['labour'] = $request->labour;
        $data['net_amount'] = $request->net_amount;
        $data['gross_amount'] = $request->gross_amount;

        if ($request->note != null) {
          $data['note'] = $request->note;
        }

        $data['sales_invoice_no'] = $request->sales_invoice_no;
        $data['type'] = $request->type;


        $data['vendor_id'] = $request->vendor_id;
        if ($request->customer_vendor_id != null) {
          $data['customer_vendor_id'] = $request->customer_vendor_id;
        } else {
          $data['customer_vendor_id'] = null;
        }
        $data['address'] = $request->address;
        $data['mobile'] = $request->mobile;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        $users->insert($data);


        $checkQty = Package::where('id', $request->select_quantity)
          ->first();

        $qtyValue = $checkQty->quantity / $checkQty->bag * $request->quantity;

        $checkValue = StockMgmt::where('commodity_id', $request->commodity_id)
          ->where('commodity_category_id', $request->commodity_category_id)
          ->first();

        $packed_stock = $checkValue->packed_stock - $qtyValue;
        $sale_stock = $checkValue->sale_stock + $qtyValue;
        if (isset($checkValue)) {
          $stockData['commodity_id'] = $request->commodity_id;
          $stockData['commodity_category_id'] = $request->commodity_category_id;
          $stockData['packed_stock'] = $packed_stock;
          $stockData['sale_stock'] =  $sale_stock;
          $stockData['updated_at'] = date('Y-m-d H:i:s');
          StockMgmt::where('id', $checkValue->id)->update($stockData);
        }



        Log::info('Sussfully added');
        toastr()->success('Data has been saved successfully!');
      } else {
        throw new Exception("Something went wrong please try again");
      }
    }

    //catch exception
    catch (Exception $e) {
      Log::error($e->getMessage());
      toastr()->error('Something went wrong please try again');
      echo 'Message: ' . $e->getMessage();
    }
    return redirect('sales');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {

    $commodity = Commodity::get();
    $user = Sales::find($id);
    $vendor = Customer::get();
    $customer = Customer::get();


    // $quantityOption = '';

    return view('sales.edit', ['user' => $user, 'commodity' => $commodity, 'vendor' => $vendor, 'customer' => $customer]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateSalesStoreRequest $request, $id)
  {

    try {
      $validated = $request->validated();
      if (!empty($validated)) {

        $data['commodity_id'] = $request->commodity_id;
        $data['commodity_category_id'] = $request->commodity_category_id;
        $data['quantity'] = $request->quantity;
        $data['rate'] = $request->rate;
        $data['commission'] = $request->commission;
        $data['transpotation'] = $request->transpotation;
        $data['labour'] = $request->labour;
        $data['net_amount'] = $request->net_amount;
        $data['gross_amount'] = $request->gross_amount;

        if ($request->note != null) {
          $data['note'] = $request->note;
        }

        $data['sales_invoice_no'] = $request->sales_invoice_no;
        $data['type'] = $request->type;

        if ($request->vendor_id != null) {
          $data['vendor_id'] = $request->vendor_id;
        }

        if ($request->customer_id != null) {
          $data['customer_id'] = $request->customer_id;
        }
        $data['address'] = $request->address;
        $data['mobile'] = $request->mobile;
        $data['updated_at'] = date('Y-m-d H:i:s');
        Sales::where('id', $id)->update($data);
        Log::info('successfully updated ');
        toastr()->success('Data has been updated successfully!');
      } else {
        throw new Exception("Something went wrong please try again");
      }
    }

    //catch exception
    catch (Exception $e) {
      Log::error($e->getMessage());
      toastr()->error('Something went wrong please try again');
      echo 'Message: ' . $e->getMessage();
    }
    return redirect('sales');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      if (!empty($id)) {
        $deleteUser = Sales::find($id);
        $deleteUser->delete();
        Log::info('successfully deleted ');
        toastr()->success('Data has been deleted successfully!');
      } else {
        toastr()->error('Something went wrong please try again');
      }
    }

    //catch exception
    catch (Exception $e) {
      Log::error($e->getMessage());
      toastr()->error('Something went wrong please try again');
      echo 'Message: ' . $e->getMessage();
    }
    return redirect('sales');
  }

  public function getpurchase($id)
  {
    $vendro = Vendor::where('id', $id)->first();
    return response()->json(["result" => "success", "status" => 200, 'html' => $vendro]);
  }

  public function getpurchasecustomer($id)
  {
    $vendro = Customer::where('id', $id)->first();
    return response()->json(["result" => "success", "status" => 200, 'html' => $vendro]);
  }
  public function getcurrentcommodity($id)
  {

    $str_arr = preg_split("/\,/", $id);
    $commodity_id = $str_arr[0];
    $commodity_category_id = $str_arr[1];
    $commodityCategory = StockMgmt::where('commodity_id', $commodity_id)
      ->where('commodity_category_id', $commodity_category_id)
      ->get();

    return response()->json(["result" => "success", "status" => 200, 'html' => $commodityCategory]);
  }

  public function getstock($id)
  {

    $str_arr = preg_split("/\,/", $id);
    $commodity_id = $str_arr[0];
    $commodity_category_id = $str_arr[1];

    $Package = Package::where('commodity_id', $commodity_id)
      ->where('commodity_category_id', $commodity_category_id)
      ->get();

    $bagStock = array();
    foreach ($Package as $val) {
      $bagStock[] = Sales::where('select_quantity', $val->id)
        ->sum('quantity');
    }


    return response()->json(["result" => "success", "status" => 200, 'html' => $Package, 'checkBag' => $bagStock]);
  }
  public function getCustomer($id)
  {
    $vendroData = Customer::where('id', $id)->first();
    $vendro = Customer::where('company_name', $vendroData->company_name)->get();
    return response()->json(["result" => "success", "status" => 200, 'html' => $vendro]);
  }


  public function checkStockQty($id)
  {

    $package = Package::where('id', $id)->select('bag')->first();
    $sales = Sales::where('select_quantity', $id)->sum('quantity');
    if ($package) {
      $sum = $package->bag - (int)$sales;

      if ($sum < 0) {
        $sumData = 0;
      } else {
        $sumData = $sum;
      }
    } else {
      $sumData = 0;
    }

    return response()->json(["result" => "success", "status" => 200, 'html' => $sumData]);
  }
}
