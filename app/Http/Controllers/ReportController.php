<?php

namespace App\Http\Controllers;
use App\Models\Purchase;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    public function purchaseReport(){

        $purchase = Purchase::paginate(15);

        return view('report.purchase',['users' => $purchase]);
    }
}
