<?php

namespace App\Http\Controllers;

use App\Models\Expanse;
use App\Models\Purchase;
use App\Models\Sales;
use App\Models\VaamUsers;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $purchase = Purchase::get()->count();
        $sales = Sales::get()->count();
        $vaamUser = VaamUsers::get()->count();
        $expanse = Expanse::get()->count();
        return view('dashboard.index',compact('purchase','sales','vaamUser','expanse'));
    }
}
