<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSalesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'commodity_id' => 'required',
            'quantity' => 'required',
            'select_quantity' => 'required',
            'rate' => 'required',
            'net_amount' => 'required',
            'commission' => 'required',
            'transpotation' => 'required',
            'labour' => 'required',
            'gross_amount' => 'required',
            'sales_invoice_no' => 'required',
            'type' => 'required',
            'address' => 'required',
            'mobile' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'commodity_id.required' => 'commodity Id is required!',
            'quantity.required' => 'Quantity is required!',
            'rate.required' => 'Rate is required!',
            'net_amount.required' => 'Net amount is required!',
            'commission.required' => 'Commission is required!',
            'transpotation.required' => 'Transpotation is required!',
            'labour.required' => 'Labour is required!',
            'gross_amount.required' => 'Gross amount is required!',
            'sales_invoice_no.required' => 'Sales invoice no is required!',
            'type.required' => 'Type is required!',
            'address.required' => 'Address is required!',
            'mobile.required' => 'Mobile no is required!',


        ];
    }
}
