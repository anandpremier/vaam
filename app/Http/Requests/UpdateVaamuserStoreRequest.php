<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVaamuserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
        'name' => 'required|string|max:50',
        'email' => 'required', 'email', \Illuminate\Validation\Rule::unique('vaamUsers')->ignore($this->user()->id),
        'mobile' => 'required|digits:10'

    ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'name.required' => 'Name is required!',
            'mobile.required' => 'mobile number is required!'
        ];
    }
}
