<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class VendorStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_type' => 'required',
            'name' => 'required|string|max:50',
           // 'email' => 'required|email|unique:vendor',
            'mobile' => 'required|digits:10',
            'address' => 'required',
           // 'account_holder' => 'required',
           // 'account_number' => 'required|digits:12',
           // 'bank_name' => 'required',
           // 'ifsc_code' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'user_type.required' => 'User Type is required!',
            'name.required' => 'Name is required!',
           // 'email.required' => 'Email is required!',
            'mobile.required' => 'Mobile number is required!',
            'address.required' => 'Address is required!',
           // 'account_holder.required' => 'Account holder is required!',
           // 'account_number.required' => 'Account number is required!',
           // 'bank_name.required' => 'Bank name is required!',
           // 'ifsc_code.required' => 'IFSC code is required!',

        ];
    }
}
