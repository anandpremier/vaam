<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ExpanseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'user_id' => 'required',
            'expanse_title' => 'required',
            'amount' => 'required',
            'date' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'User Id is required!',
            'expanse_title.required' => 'Name is required!',
            'amount.required' => 'Amount is required!',
            'date.required' => 'Date is required!',

        ];
    }
}
