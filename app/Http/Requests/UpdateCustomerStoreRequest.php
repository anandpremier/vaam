<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
          //  'company_name' => 'required|string|max:50',
            'name' => 'required|string|max:50',
         //   'email' => 'required', 'email', \Illuminate\Validation\Rule::unique('customer')->ignore($this->user()->id),
            'mobile' => 'required|digits:10',
            'address' => 'required',
          //  'account_holder' => 'required',
          //  'account_number' => 'required|digits:12',
          //  'bank_name' => 'required',
          //  'ifsc_code' => 'required',

        ];
    }

    public function messages()
    {
        return [
           // 'company_name.required' => 'Company name is required!',
            'name.required' => 'Name is required!',
           // 'email.required' => 'Email is required!',
            'mobile.required' => 'mobile number is required!',
            'address.required' => 'Address is required!',
          //  'account_holder.required' => 'Account holder is required!',
           // 'account_number.required' => 'Account number is required!',
           // 'bank_name.required' => 'Bank name is required!',
           // 'ifsc_code.required' => 'IFSC code is required!',

        ];
    }
}
