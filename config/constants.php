<?php
return [

	'userType' => array(
		'1' => 'Comapny',
		'2'	=> 'Farmer'
    ),
    'packageSize' => array(
        '250' => '250 GM',
        '500' => '500 GM',
        '1000' => '1 KG',
        '5000' => '5 KG',
        '10000' => '10 KG',
        '20000' => '20 KG',
        '25000' => '25 KG',
        '50000' => '50 KG',
    ),


];
?>
