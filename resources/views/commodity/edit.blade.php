@extends('admin.main')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Commodity</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="commodityFrom" action="{{ route('commodity.update', ['id' => $user->id]) }}" class="form-horizontal" method="POST">

                @csrf
                @method('POST')
               @include('commodity.form')
            </form>
        </div>

        <div class="card">
            <div class="card-header">

            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vaamUsers" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Name</th>
                <th>Action</th>
                </tr>
                </thead>
                <tbody>

                    @foreach ($commodityQtys as $commodityQty)
                    <tr>

                        {{-- <?php //$commodity =  //\App\Models\Commodity::where('id',$user->commodity_id)->first(); ?>
                        <td> {{ $commodity->name }}</td> --}}
                        <td> {{ $commodityQty->name }}</td>

                        <td> {{--<a href="{{ route('commodity.category.edit', ['id' => $user->id]) }}"><i class="fa fa-edit"></i></a> --}}
                            <a href="{{ route('commodity.category.delete', ['id' => $commodityQty->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                </tbody>

              </table>

              {{$commodityQtyPaginate->links() }}
            </div>
            <!-- /.card-body -->
          </div>


    </div>
</div>
</div>
</div>
</div>

@endsection
