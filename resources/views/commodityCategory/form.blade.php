

              <div class="card-body">

                <div class="form-group @if ($errors->has('commodity_id')) has-error @endif">

                    <input type="hidden" class="form-control" name="commodity_id" id="commodity_id" placeholder="Enter name"
                    value="{{ !empty(old('commodity_id')) ? old('commodity_id') : ((!empty($user) && !empty($user->commodity_id)) ? $user->commodity_id : $commodity) }}"  /> @if ($errors->has('commodity_id'))
                      <span class="text-danger">{{ $errors->first('commodity_id') }}</span> @endif


                  @if ($errors->has('commodity_id'))
                      <span class="text-danger">{{ $errors->first('commodity_id') }}</span>
                  @endif


                  </div>

                <div class="form-group @if ($errors->has('name')) has-error @endif">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" name="name" id="name" placeholder="Enter name"
                  value="{{ !empty(old('name')) ? old('name') : ((!empty($user) && !empty($user->name)) ? $user->name : "") }}"  /> @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span> @endif
                </div>

              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

