@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">

            <a class="btn btn-primary float-right" href="{{ url('commodity-category/create') }}">New Commodity Category</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vaamUsers" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>commodity</th>
                  <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($commodityCategory as $user)
                    <tr>

                        <?php $commodity =  \App\Models\Commodity::where('id',$user->commodity_id)->first(); ?>
                        <td> {{ $commodity->name }}</td>
                        <td> {{ $user->name }}</td>

                        <td><a href="{{ route('commodity.category.edit', ['id' => $user->id]) }}"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('commodity.category.delete', ['id' => $user->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                </tbody>

              </table>

              {{ $commodityCategory->links() }}
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
