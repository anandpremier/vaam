

              <div class="card-body">

                <div class="form-group @if ($errors->has('name')) has-error @endif">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" name="name" id="name" placeholder="Enter name"
                  value="{{ !empty(old('name')) ? old('name') : ((!empty($user) && !empty($user->name)) ? $user->name : "") }}"  /> @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span> @endif
                </div>

                <div class="form-group @if ($errors->has('email')) has-error @endif">
                  <label for="email">Email</label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ !empty(old('email')) ? old('email') : ((!empty($user) && !empty($user->email)) ? $user->email : "") }}"  /> @if ($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span> @endif
                </div>

                <div class="form-group @if ($errors->has('mobile')) has-error @endif">
                    <label for="mobile">Mobile Number</label>
                    <input type="mobile" name="mobile" class="form-control" id="mobile" placeholder="Enter mobile number" value="{{ !empty(old('mobile')) ? old('mobile') : ((!empty($user) && !empty($user->mobile)) ? $user->mobile : "") }}"  /> @if ($errors->has('mobile'))
                    <span class="text-danger">{{ $errors->first('mobile') }}</span> @endif
                  </div>


              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

