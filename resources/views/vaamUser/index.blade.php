@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">

            <a class="btn btn-primary float-right" href="{{ url('vaamUsers/create') }}">New User</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vaamUsers" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile Number</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td> {{ $user->name }}</td>
                        <td> {{ $user->email }}</td>
                        <td> {{ $user->mobile}}</td>
                        <td><a href="{{ route('vaam.edit', ['id' => $user->id]) }}"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('vaam.delete', ['id' => $user->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                </tbody>

              </table>

              {{ $users->links() }}
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
