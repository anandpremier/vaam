@extends('admin.main')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Purchase</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="purchaseForm" action="{{ route('purchase.update', ['id' => $user->id]) }}" class="form-horizontal" method="POST">

                @csrf
                @method('POST')
                <div class="card-body">
                    <div class="form-group @if ($errors->has('commodity_id')) has-error @endif">
                    <label for="commodity_id">Select commodity</label>

                    <select  name="commodity_id" id="commodity_id" class="form-control">
                      <option selected disabled>Please select Commodity</option>
                      @foreach ($commodity as $key => $value)
                      <option value="{{ $value->id }}"
                      {{ !empty(old('commodity_id')) && old('commodity_id') == $value->id
                          ? 'selected="selected"'
                          : ((!empty($user) && !empty($user->commodity_id)) && $user->commodity_id == $value->id
                              ? 'selected="selected"'
                              : "")
                      }}>{{ $value->name }}</option>
                  @endforeach

                  </select>
                  @if ($errors->has('commodity_id'))
                      <span class="text-danger">{{ $errors->first('commodity_id') }}</span>
                  @endif


                  </div>

                  <div class="form-group @if ($errors->has('commodity_category_id')) has-error @endif">
                      <label for="commodity_category_id">Select commodity category</label>

                      <select  name="commodity_category_id" id="commodity_category_id" class="form-control">
                        <option selected disabled>Please select commodity category</option>
                        @if(isset($commodityCategory))
                        @foreach ($commodityCategory as $key => $value)
                        <option value="{{ $value->id }}"
                        {{ !empty(old('commodity_category_id')) && old('commodity_category_id') == $value->id
                            ? 'selected="selected"'
                            : ((!empty($user) && !empty($user->commodity_category_id)) && $user->commodity_category_id == $value->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $value->name }}</option>
                          @endforeach
                      @endif

                    </select>
                    @if ($errors->has('commodity_category_id'))
                        <span class="text-danger">{{ $errors->first('commodity_category_id') }}</span>
                    @endif


                    </div>



                  <div class="form-group @if ($errors->has('quantity')) has-error @endif">
                      <label for="quantity">Quantity</label>
                      <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Enter quantity"
                      value="{{ !empty(old('quantity')) ? old('quantity') : ((!empty($user) && !empty($user->quantity)) ? $user->quantity : "") }}"  /> @if ($errors->has('quantity'))
                        <span class="text-danger">{{ $errors->first('quantity') }}</span> @endif
                  </div>

                  <div class="form-group @if ($errors->has('rate')) has-error @endif">
                      <label for="rate">Rate</label>
                      <input type="number" class="form-control" name="rate" id="rate" placeholder="Enter rate"
                      value="{{ !empty(old('rate')) ? old('rate') : ((!empty($user) && !empty($user->rate)) ? $user->rate : "") }}"  /> @if ($errors->has('rate'))
                        <span class="text-danger">{{ $errors->first('rate') }}</span> @endif
                  </div>


                  <div class="form-group @if ($errors->has('net_amount')) has-error @endif">
                    <label for="net_amount">Net amount</label>
                    <input type="text" readonly class="form-control" name="net_amount" id="net_amount" placeholder="Enter Net amount"
                    value="{{ !empty(old('net_amount')) ? old('net_amount') : ((!empty($user) && !empty($user->net_amount)) ? $user->net_amount : "") }}"  /> @if ($errors->has('net_amount'))
                      <span class="text-danger">{{ $errors->first('net_amount') }}</span> @endif
                  </div>


                  <div class="form-group @if ($errors->has('commission')) has-error @endif">
                    <label for="commission">Commission</label>
                    <input type="number" class="form-control" name="commission" id="commission" placeholder="Enter commission"
                    value="{{ !empty(old('commission')) ? old('commission') : ((!empty($user) && !empty($user->commission)) ? $user->commission : "") }}"  /> @if ($errors->has('commission'))
                      <span class="text-danger">{{ $errors->first('commission') }}</span> @endif
                  </div>

                  <div class="form-group @if ($errors->has('transpotation')) has-error @endif">
                      <label for="transpotation">Transpotation</label>
                      <input type="number" class="form-control" name="transpotation" id="transpotation" placeholder="Enter transpotation"
                      value="{{ !empty(old('transpotation')) ? old('transpotation') : ((!empty($user) && !empty($user->transpotation)) ? $user->transpotation : "") }}"  /> @if ($errors->has('transpotation'))
                        <span class="text-danger">{{ $errors->first('transpotation') }}</span> @endif
                    </div>

                    <div class="form-group @if ($errors->has('labour')) has-error @endif">
                      <label for="labour">Labour</label>
                      <input type="number" class="form-control" name="labour" id="labour" placeholder="Enter labour"
                      value="{{ !empty(old('labour')) ? old('labour') : ((!empty($user) && !empty($user->labour)) ? $user->labour : "") }}"  /> @if ($errors->has('labour'))
                        <span class="text-danger">{{ $errors->first('labour') }}</span> @endif
                    </div>

                    <div class="form-group @if ($errors->has('gross_amount')) has-error @endif">
                      <label for="gross_amount">Gross amount</label>
                      <input type="number" class="form-control" name="gross_amount" id="gross_amount" placeholder="Enter gross amount"
                      value="{{ !empty(old('gross_amount')) ? old('gross_amount') : ((!empty($user) && !empty($user->gross_amount)) ? $user->gross_amount : "") }}"  readonly /> @if ($errors->has('gross_amount'))
                        <span class="text-danger">{{ $errors->first('gross_amount') }}</span> @endif
                    </div>

                    <div class="form-group @if ($errors->has('note')) has-error @endif">
                      <label for="note">Note</label>
                      <textarea class="form-control"  name="note" id="note" rows="3" placeholder="Enter note">{{ !empty(old('note')) ? old('note') : ((!empty($user) && !empty($user->note)) ? $user->note : "") }}</textarea>
                      @if ($errors->has('note'))
                        <span class="text-danger">{{ $errors->first('note') }}</span> @endif
                    </div>

                    <div class="form-group @if ($errors->has('purchase_invoice_no')) has-error @endif">
                      <label for="purchase_invoice_no">Purchase invoice no</label>
                      <input type="text" class="form-control" name="purchase_invoice_no" id="purchase_invoice_no" placeholder="Enter purchase invoice no"
                      value="{{ !empty(old('purchase_invoice_no')) ? old('purchase_invoice_no') : ((!empty($user) && !empty($user->purchase_invoice_no)) ? $user->purchase_invoice_no : "") }}"  /> @if ($errors->has('purchase_invoice_no'))
                        <span class="text-danger">{{ $errors->first('purchase_invoice_no') }}</span> @endif
                    </div>

                    <div class="form-group @if ($errors->has('type')) has-error @endif">
                      <label for="purchase_invoice_no">Type</label>
                      <div class="form-check">

                        <input class="form-check-input" type="radio" value="1" @if(isset($user)) @if($user->type == 1) checked  @endif @endif  name="type">
                        <label class="form-check-label">Company</label>
                      </br>
                        <input class="form-check-input" type="radio" value="0" @if(isset($user)) @if($user->type == 0) checked  @endif @endif name="type">
                          <label class="form-check-label">Direct farmer</label>
                      </div>
                      @if ($errors->has('type'))
                        <span class="text-danger">{{ $errors->first('type') }}</span> @endif


                    </div>


                    @if($user->type == 1)
                    <div class="form-group @if ($errors->has('vendor_id')) has-error @endif vendorClass">
                      <label for="vendor_id">Company</label>
                      <select  name="company_vendor_id" id="purchase_vendor_id" class="form-control">
                          <option selected disabled>Please select company</option>
                          @foreach ($vendor as $key => $value)

                          <option value="{{ $value->id }}"
                          {{ !empty(old('vendor_id')) && old('vendor_id') == $value->id
                              ? 'selected="selected"'
                              : ((!empty($user) && !empty($user->vendor_id)) && $user->vendor_id == $value->id
                                  ? 'selected="selected"'
                                  : "")
                          }}>{{ $value->company_name }}</option>
                      @endforeach

                      </select>
                      @if ($errors->has('vendor_id'))
                          <span class="text-danger">{{ $errors->first('vendor_id') }}</span>
                      @endif


                    </div>

                    <div class="form-group @if ($errors->has('vendor_id')) has-error @endif vendorClass" >
                      <label for="vendor_id">Company user</label>

                      <select  name="vendor_id" id="vendor_user_id" class="form-control">
                        <option selected disabled>Please select company user</option>

                        @if(isset($vendorCompany))
                        @foreach ($vendorCompany as $key => $value)
                        <option value="{{ $value->id }}"
                        {{ !empty(old('vendor_id')) && old('vendor_id') == $value->id
                            ? 'selected="selected"'
                            : ((!empty($user) && !empty($user->vendor_id)) && $user->vendor_id == $value->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $value->name }}</option>
                          @endforeach
                      @endif

                    </select>
                    @if ($errors->has('vendor_id'))
                        <span class="text-danger">{{ $errors->first('vendor_id') }}</span>
                    @endif


                    </div>

                    <div class="form-group @if ($errors->has('vendor_id')) has-error @endif customerClass" style="display:none;" >
                        <label for="vendor_id">Person</label>

                        <select  name="vendor_id" id="customer_id" class="form-control">

                        <option selected disabled>Please select Person</option>
                          @foreach ($customer as $key => $value)

                          <option value="{{ $value->id }}"
                          {{ !empty(old('vendor_id')) && old('vendor_id') == $value->id
                              ? 'selected="selected"'
                              : ((!empty($user) && !empty($user->vendor_id)) && $user->vendor_id == $value->id
                                  ? 'selected="selected"'
                                  : "")
                          }}>{{ $value->name }}</option>
                      @endforeach

                      </select>
                      @if ($errors->has('vendor_id'))
                          <span class="text-danger">{{ $errors->first('vendor_id') }}</span>
                      @endif


                      </div>
                    @endif

                    @if($user->type == 0)


                    <div class="form-group @if ($errors->has('vendor_id')) has-error @endif vendorClass" style="display: none;">
                        <label for="vendor_id">Company</label>
                        <select  name="company_vendor_id" id="purchase_vendor_id" class="form-control">
                            <option selected disabled>Please select company</option>
                            @foreach ($vendor as $key => $value)

                            <option value="{{ $value->id }}"
                            {{ !empty(old('vendor_id')) && old('vendor_id') == $value->id
                                ? 'selected="selected"'
                                : ((!empty($user) && !empty($user->vendor_id)) && $user->vendor_id == $value->id
                                    ? 'selected="selected"'
                                    : "")
                            }}>{{ $value->company_name }}</option>
                        @endforeach

                        </select>
                        @if ($errors->has('vendor_id'))
                            <span class="text-danger">{{ $errors->first('vendor_id') }}</span>
                        @endif


                      </div>

                      <div class="form-group @if ($errors->has('vendor_id')) has-error @endif vendorClass" style="display: none;">
                        <label for="vendor_id">Company user</label>

                        <select  name="vendor_id" id="vendor_user_id" class="form-control">
                          <option selected disabled>Please select company user</option>

                          @if(isset($vendorCompany))
                          @foreach ($vendorCompany as $key => $value)
                          <option value="{{ $user->id }}"
                          {{ !empty(old('vendor_id')) && old('vendor_id') == $value->id
                              ? 'selected="selected"'
                              : ((!empty($user) && !empty($user->vendor_id)) && $user->vendor_id == $value->id
                                  ? 'selected="selected"'
                                  : "")
                          }}>{{ $value->name }}</option>
                            @endforeach
                        @endif

                      </select>
                      @if ($errors->has('vendor_id'))
                          <span class="text-danger">{{ $errors->first('vendor_id') }}</span>
                      @endif


                      </div>

                    <div class="form-group @if ($errors->has('vendor_id')) has-error @endif customerClass" >
                      <label for="vendor_id">Person</label>

                      <select  name="vendor_id" id="customer_id" class="form-control">

                      <option selected disabled>Please select Person</option>
                        @foreach ($customer as $key => $value)

                        <option value="{{ $value->id }}"
                        {{ !empty(old('vendor_id')) && old('vendor_id') == $value->id
                            ? 'selected="selected"'
                            : ((!empty($user) && !empty($user->vendor_id)) && $user->vendor_id == $value->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $value->name }}</option>
                    @endforeach

                    </select>
                    @if ($errors->has('vendor_id'))
                        <span class="text-danger">{{ $errors->first('vendor_id') }}</span>
                    @endif


                    </div>
                    @endif


                    <div class="form-group @if ($errors->has('address')) has-error @endif">
                      <label for="address">Address</label>
                      <textarea readonly class="form-control" name="address" id="address" rows="3" placeholder="Enter address">{{ !empty(old('address')) ? old('address') : ((!empty($user) && !empty($user->address)) ? $user->address : "") }}</textarea>
                      @if ($errors->has('address'))
                        <span class="text-danger">{{ $errors->first('address') }}</span> @endif
                    </div>

                    <div class="form-group @if ($errors->has('mobile')) has-error @endif">
                      <label for="mobile">Mobile</label>
                      <input readonly type="number" class="form-control" name="mobile" id="mobile" placeholder="Enter mobile"
                      value="{{ !empty(old('mobile')) ? old('mobile') : ((!empty($user) && !empty($user->mobile)) ? $user->mobile : "") }}"  /> @if ($errors->has('mobile'))
                        <span class="text-danger">{{ $errors->first('mobile') }}</span> @endif
                    </div>



          </div>

            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>

            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>

@endsection
