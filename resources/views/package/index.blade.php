@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">

            <a class="btn btn-primary float-right" href="{{ url('package/create') }}">New Package</a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
                <table id="vaamUsers" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                  <th>Commodity</th>
                  <th>Commodity category</th>
                  <th>Date</th>
                  <th>Bag Size</th>
                  <th>Bag</th>

                  <th>Quantity</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($recodes as $recode)
                    <tr>
                        <?php $recodeInfo =  \App\Models\Commodity::where('id',$recode->commodity_id)->first(); ?>
                        <td>{{ $recodeInfo->name }}</td>
                        <?php $commdityCategoryInfo =  \App\Models\Commodity_category::where('id',$recode->commodity_category_id)->first(); ?>
                        <td>{{ $commdityCategoryInfo->name }}</td>
                        <td> {{ $recode->date }}</td>
                        <?php if($recode->bag_size == 250){
                            $bagSize = '250GM';
                        }elseif($recode->bag_size == 500){
                            $bagSize = '500GM';
                        }elseif($recode->bag_size == 1000){
                            $bagSize = '1KG';
                        }elseif($recode->bag_size == 5000){
                            $bagSize = '5KG';
                        }elseif($recode->bag_size == 10000){
                            $bagSize = '10KG';
                        }elseif($recode->bag_size == 20000){
                            $bagSize = '20KG';
                        }elseif($recode->bag_size == 25000){
                            $bagSize = '25KG';
                        }elseif($recode->bag_size == 50000){
                            $bagSize = '50KG';
                        }
                         ?>
                        <td>{{ $bagSize }}</td>
                        <td>{{ $recode->bag }}</td>
                        <td> {{ $recode->quantity }}</td>
                        <td>
                            <a href="{{ route('package.delete', ['id' => $recode->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                  </tbody>

                </table>
                {{ $recodes->links() }}
              </div>

            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
