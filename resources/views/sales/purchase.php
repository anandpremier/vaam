@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">

            <a class="btn btn-primary float-right" href="{{ url('sales/create') }}">New Sales</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vaamUsers" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Commodity</th>
                  <th>Quantity</th>
                  <th>Gross amount</th>
                  <th>Sales invoice no</th>
                  <th>Type</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <?php $userInfo =  \App\Models\Commodity::where('id',$user->commodity_id)->first(); ?>
                        <td>{{ $userInfo->name }}</td>
                        <td>{{ $user->quantity }}</td>
                        <td> {{ $user->gross_amount }}</td>
                        <td> {{ $user->sales_invoice_no }}</td>
                        <td>
                            @if($user->type == 1)
                            Company
                            @else
                            Farmer
                            @endif
                        </td>

                        <td><a href="{{ route('sales.edit', ['id' => $user->id]) }}"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('sales.delete', ['id' => $user->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                </tbody>

              </table>

              {{ $users->links() }}
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
