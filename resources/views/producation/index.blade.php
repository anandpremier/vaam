@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">

            <a class="btn btn-primary float-right" href="{{ url('producation/create') }}">New Production</a>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
                <table id="vaamUsers" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                  <th>Commodity</th>
                  <th>Commodity category</th>
                  <th>Date</th>
                  <th>Quantity</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($recodes as $recode)
                    <tr>
                        <?php $recodeInfo =  \App\Models\Commodity::where('id',$recode->commodity_id)->first(); ?>
                        <td>{{ $recodeInfo->name }}</td>
                        <?php $commdityCategoryInfo =  \App\Models\Commodity_category::where('id',$recode->commodity_category_id)->first(); ?>
                        <td>{{ $commdityCategoryInfo->name }}</td>
                        <td> {{ $recode->date }}</td>
                        <td> {{ $recode->quantity }}</td>
                        <td>
                            <a href="{{ route('producation.delete', ['id' => $recode->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                  </tbody>

                </table>
                {{ $recodes->links() }}
              </div>

            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
