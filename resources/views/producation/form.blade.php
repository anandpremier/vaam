

              <div class="card-body">
                      <div class="form-group @if ($errors->has('commodity_id')) has-error @endif">
                      <label for="commodity_id">Commodity</label>

                      <select  name="commodity_id" id="producation_commodity_id" class="form-control">
                        <option selected disabled>Please select Commodity</option>
                        @foreach ($commodity as $key => $value)
                        <option value="{{ $value->id }}"
                        {{ !empty(old('commodity_id')) && old('commodity_id') == $value->id
                            ? 'selected="selected"'
                            : ((!empty($user) && !empty($user->commodity_id)) && $user->commodity_id == $value->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $value->name }}</option>
                    @endforeach

                    </select>
                    @if ($errors->has('commodity_id'))
                        <span class="text-danger">{{ $errors->first('commodity_id') }}</span>
                    @endif


                    </div>

                    <div class="form-group @if ($errors->has('commodity_category_id')) has-error @endif">
                        <label for="commodity_category_id">Commodity Quality</label>

                        <select  name="commodity_category_id" id="producation_commodity_category_id" class="form-control producation_commodity_category_id">
                          <option selected disabled>Please select Commodity Quality</option>

                          @if(isset($commodityCategory))
                          @foreach ($commodityCategory as $key => $value)
                          <option value="{{ $user->id }}"
                          {{ !empty(old('commodity_category_id')) && old('commodity_category_id') == $value->id
                              ? 'selected="selected"'
                              : ((!empty($user) && !empty($user->commodity_category_id)) && $user->commodity_category_id == $value->id
                                  ? 'selected="selected"'
                                  : "")
                          }}>{{ $value->name }}</option>
                            @endforeach
                        @endif

                      </select>
                      @if ($errors->has('commodity_category_id'))
                          <span class="text-danger">{{ $errors->first('commodity_category_id') }}</span>
                      @endif


                      </div>

                      <div class="form-group @if ($errors->has('date')) has-error @endif">
                        <label for="date">Date:</label>
                        <input type="text" name="date" id="date" class="form-control datetimepicker-input" value="{{ !empty(old('date')) ? old('date') : ((!empty($user) && !empty($user->date)) ? $user->date : "") }}"/>
                          @if ($errors->has('date'))
                        <span class="text-danger">{{ $errors->first('date') }}</span> @endif
                      </div>

                    <div class="form-group @if ($errors->has('quantity')) has-error @endif">
                        <label class="col-12 float-left" for="quantity">Quantity</label>
                        <div class="col-10 float-left">
                        <input type="text" class="form-control producationQuantity" name="quantity" id="quantity" placeholder="Enter quantity"
                        value="{{ !empty(old('quantity')) ? old('quantity') : ((!empty($user) && !empty($user->quantity)) ? $user->quantity : "") }}"  /> @if ($errors->has('quantity'))
                          <span class="text-danger">{{ $errors->first('quantity') }}</span> @endif
                        </div>
                        <div class="col-2 float-left">
                            <input type="text" readonly class="form-control" name="currentQuantity" id="currentQuantity"

                        value=" @if(isset($user->stockMgmts->purchase_stock)) {{ $user->stockMgmts->purchase_stock }} @else {{ !empty(old('currentQuantity')) ? old('currentQuantity') : ((!empty($user) && !empty($user->stockMgmts->purchase_stock)) ? $user->stockMgmts->purchase_stock : "") }} @endif"   /> @if ($errors->has('currentQuantity'))
                              <span class="text-danger">{{ $errors->first('currentQuantity') }}</span> @endif

                        </div>
                    </div>


            </div>

              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
