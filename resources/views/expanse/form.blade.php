

              <div class="card-body">


                    <div class="form-group @if ($errors->has('user_id')) has-error @endif">
                      <label for="user_id">User</label>

                      <select  name="user_id" id="user_id" class="form-control">
                        <option selected disabled>Please select User</option>
                        @foreach ($vaamUser as $key => $value)
                        <option value="{{ $value->id }}"
                        {{ !empty(old('user_id')) && old('user_id') == $value->id
                            ? 'selected="selected"'
                            : ((!empty($user) && !empty($user->user_id)) && $user->user_id == $value->id
                                ? 'selected="selected"'
                                : "")
                        }}>{{ $value->name }}</option>
                    @endforeach

                    </select>
                    @if ($errors->has('user_id'))
                        <span class="text-danger">{{ $errors->first('user_id') }}</span>
                    @endif


                    </div>



                    <div class="form-group @if ($errors->has('expanse_title')) has-error @endif">
                      <label for="expanse_title">Expanse title</label>
                      <input type="text" class="form-control" name="expanse_title" id="expanse_title" placeholder="Enter expanse title"
                      value="{{ !empty(old('expanse_title')) ? old('expanse_title') : ((!empty($user) && !empty($user->expanse_title)) ? $user->expanse_title : "") }}"  /> @if ($errors->has('expanse_title'))
                        <span class="text-danger">{{ $errors->first('expanse_title') }}</span> @endif
                    </div>


                    <div class="form-group @if ($errors->has('amount')) has-error @endif">
                      <label for="amount">Amount</label>
                      <input type="number" class="form-control" name="amount" id="amount" placeholder="Enter amount"
                      value="{{ !empty(old('amount')) ? old('amount') : ((!empty($user) && !empty($user->amount)) ? $user->amount : "") }}"  /> @if ($errors->has('amount'))
                        <span class="text-danger">{{ $errors->first('amount') }}</span> @endif
                    </div>


                    <div class="form-group @if ($errors->has('date')) has-error @endif">
                        <label for="date">Date:</label>
                        <input type="text" name="date" id="date" class="form-control datetimepicker-input" value="{{ !empty(old('date')) ? old('date') : ((!empty($user) && !empty($user->date)) ? $user->date : "") }}"/>
                          @if ($errors->has('date'))
                        <span class="text-danger">{{ $errors->first('date') }}</span> @endif
                      </div>
            </div>

              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

