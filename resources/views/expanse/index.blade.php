@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">

            <a class="btn btn-primary float-right" href="{{ url('expanse/create') }}">New Expanse</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vaamUsers" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User</th>
                  <th>Expanse title</th>
                  <th>amount</th>
                  <th>Date</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <?php $userInfo =  \App\Models\VaamUsers::where('id',$user->user_id)->first(); ?>
                        <td>{{ $userInfo->name }}</td>
                        <td> {{ $user->expanse_title }}</td>
                        <td> {{ $user->amount }}</td>
                        <td> {{ $user->date }}</td>

                        <td><a href="{{ route('expanse.edit', ['id' => $user->id]) }}"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('expanse.delete', ['id' => $user->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                </tbody>

              </table>

              {{ $users->links() }}
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
