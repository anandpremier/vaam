

              <div class="card-body">


                <div class="form-group @if ($errors->has('user_type')) has-error @endif">
                    <label for="user_type">User Type</label>

                    <select  name="user_type" id="user_type_vendor" class="form-control">
                      <option selected disabled>Please select User Type</option>
                      @foreach ($userType as $key => $value)
                      <option value="{{ $key }}"
                      {{ !empty(old('user_type')) && old('user_type') == $key
                          ? 'selected="selected"'
                          : ((!empty($user) && !empty($user->user_type)) && $user->user_type == $key
                              ? 'selected="selected"'
                              : "")
                      }}>{{ $value }}</option>
                  @endforeach

                  </select>
                  @if ($errors->has('user_type'))
                      <span class="text-danger">{{ $errors->first('user_type') }}</span>
                  @endif


                  </div>

                    <div class="form-group @if ($errors->has('company_name')) has-error @endif showVendorType" @if(isset($user)) @if($user->company_name == null) style="display: none;" @else style="display:block;" @endif @else style="display:none;" @endif>
                      <label for="company_name">Company Name</label>
                      <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Enter Company Name"
                      value="{{ !empty(old('company_name')) ? old('company_name') : ((!empty($user) && !empty($user->company_name)) ? $user->company_name : "") }}"  /> @if ($errors->has('company_name'))
                        <span class="text-danger">{{ $errors->first('company_name') }}</span> @endif
                    </div>



                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                      <label for="name">Name</label>
                      <input type="text" class="form-control" name="name" id="name" placeholder="Enter name"
                      value="{{ !empty(old('name')) ? old('name') : ((!empty($user) && !empty($user->name)) ? $user->name : "") }}"  /> @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span> @endif
                    </div>



                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" name="email" id="email" placeholder="Enter email"
                      value="{{ !empty(old('email')) ? old('email') : ((!empty($user) && !empty($user->email)) ? $user->email : "") }}"  /> @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span> @endif
                    </div>



                    <div class="form-group @if ($errors->has('mobile')) has-error @endif">
                      <label for="mobile">Mobile</label>
                      <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Enter mobile"
                      value="{{ !empty(old('mobile')) ? old('mobile') : ((!empty($user) && !empty($user->mobile)) ? $user->mobile : "") }}"  /> @if ($errors->has('mobile'))
                        <span class="text-danger">{{ $errors->first('mobile') }}</span> @endif
                    </div>



                    <div class="form-group @if ($errors->has('address')) has-error @endif">
                      <label for="address">Address</label>
                      <input type="text" class="form-control" name="address" id="address" placeholder="Enter address"
                      value="{{ !empty(old('address')) ? old('address') : ((!empty($user) && !empty($user->address)) ? $user->address : "") }}"  /> @if ($errors->has('address'))
                        <span class="text-danger">{{ $errors->first('address') }}</span> @endif
                    </div>



                    <div class="form-group @if ($errors->has('account_holder')) has-error @endif">
                      <label for="account_holder">Account Holder</label>
                      <input type="text" class="form-control" name="account_holder" id="account_holder" placeholder="Enter account holder"
                      value="{{ !empty(old('account_holder')) ? old('account_holder') : ((!empty($user) && !empty($user->account_holder)) ? $user->account_holder : "") }}"  /> @if ($errors->has('account_holder'))
                        <span class="text-danger">{{ $errors->first('account_holder') }}</span> @endif
                    </div>


                <div class="form-group @if ($errors->has('account_number')) has-error @endif">
                  <label for="account_number">Account number</label>
                  <input type="text" class="form-control" name="account_number" id="account_number" placeholder="Enter account number"
                  value="{{ !empty(old('account_number')) ? old('account_number') : ((!empty($user) && !empty($user->account_number)) ? $user->account_number : "") }}"  /> @if ($errors->has('account_number'))
                    <span class="text-danger">{{ $errors->first('account_number') }}</span> @endif

            </div>


                <div class="form-group @if ($errors->has('bank_name')) has-error @endif">
                  <label for="bank_name">Bank name</label>
                  <input type="text" class="form-control" name="bank_name" id="bank_name" placeholder="Enter bank name"
                  value="{{ !empty(old('bank_name')) ? old('bank_name') : ((!empty($user) && !empty($user->bank_name)) ? $user->bank_name : "") }}"  /> @if ($errors->has('bank_name'))
                    <span class="text-danger">{{ $errors->first('bank_name') }}</span> @endif
                </div>



                <div class="form-group @if ($errors->has('ifsc_code')) has-error @endif">
                  <label for="ifsc_code">	IFSC code</label>
                  <input type="text" class="form-control" name="ifsc_code" id="ifsc_code" placeholder="Enter IFSC code"
                  value="{{ !empty(old('ifsc_code')) ? old('ifsc_code') : ((!empty($user) && !empty($user->ifsc_code)) ? $user->ifsc_code : "") }}"  /> @if ($errors->has('ifsc_code'))
                    <span class="text-danger">{{ $errors->first('ifsc_code') }}</span> @endif
                </div>
            </div>

              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

