<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
      <img src="{{ asset('dist/img/logo.png') }}" alt="Vaam Agro" class="brand-image img-circle elevation-3"
           style="opacity: .8">
     </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
          <a href="{{ route('home') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard

              </p>
            </a>

          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Commodity Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('commodity') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Commodity</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{ url('purchase') }}" class="nav-link">
              <i class="nav-icon fa fa-credit-card"></i>
              <p>
                Purchase management
             </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-industry"></i>
              <p>
                Production Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('producation') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Production</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('package') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Packed Material</p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-item">
            <a href="{{ url('sales') }}" class="nav-link">
              <i class="nav-icon fa fa-credit-card"></i>
              <p>
                Sales Management
             </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('expanse') }}" class="nav-link">
              <i class="nav-icon fa fa-address-card"></i>
              <p>
                Expanse Management
             </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('vendor') }}" class="nav-link">
              <i class="nav-icon fa fa-truck"></i>
              <p>
                Vendor Management
             </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('customer') }}" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Customer Management
             </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('vaamUsers') }}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User Management
             </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('purchaseReport') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchase Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/flot.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sell Report</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/inline.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Expanse Report</p>
                </a>
              </li>
            </ul>
          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
