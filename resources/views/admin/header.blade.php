  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <div class="logOutBtn">
         <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button class="btn btn-danger"  style="float: right;" type="submit"><i class="fa fa-power-off" aria-hidden="true"></i></button>
              </form>
            </div>
  </nav>
  <!-- /.navbar -->
