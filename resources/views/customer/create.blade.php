@extends('admin.main')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Customer</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="customerForm" action="{{ route('customer.store') }}" class="form-horizontal" method="POST">

                @csrf
                @method('POST')
               @include('customer.form')
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>

@endsection
