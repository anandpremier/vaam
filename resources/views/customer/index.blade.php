@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">

            <a class="btn btn-primary float-right" href="{{ url('customer/create') }}">New Customer</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="vaamUsers" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>User Type</th>
                  <th>Company Name</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Bank Name</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <?php if($user->user_type == 1){
                             $type = 'Comapny';
                        }else{
                            $type="Farmer";
                        } ?>
                        <td>{{ $type }}</td>
                        <td> {{ $user->company_name }}</td>
                        <td> {{ $user->name }}</td>
                        <td> {{ $user->email }}</td>
                        <td> {{ $user->mobile }}</td>
                        <td> {{ $user->bank_name }}</td>

                        <td><a href="{{ route('customer.edit', ['id' => $user->id]) }}"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('customer.delete', ['id' => $user->id]) }}"><i class="fa fa-trash-alt"></i></a></td>
                    </tr>

                @endforeach

                </tbody>

              </table>

              {{ $users->links() }}
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
