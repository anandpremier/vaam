@extends('admin.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <div class="col-12">
        <div class="card">

            <!-- /.card-header -->
            <div class="card-body">
              <table id="purchaseReport" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Puchase Invoice No.</th>
                  <th>Date</th>
                  <th>Party Name</th>
                  <th>Commodity</th>
                  <th>Rate</th>
                  <th>Quantity</th>
                  <th>Net Rate</th>
                  <th>Gross amount</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td> {{ $user->purchase_invoice_no }}</td>
                        <td> <?php echo date('d-m-Y', strtotime($user->created_at)); ?></td>

                        <?php $vendorInfo =  \App\Models\Vendor::where('id',$user->vendor_id)->first(); ?>
                        <td>{{ $vendorInfo->name }}</td>
                        <?php $userInfo =  \App\Models\Commodity::where('id',$user->commodity_id)->first(); ?>
                        <td>{{ $userInfo->name }}</td>
                        <td>{{ $user->rate}}</td>
                        <td>{{ $user->quantity }}</td>
                        <td>{{ $user->net_amount }}</td>
                        <td> {{ $user->gross_amount }}</td>

 </tr>

                @endforeach

                </tbody>

              </table>

              {{ $users->links() }}
            </div>
            <!-- /.card-body -->
          </div>
      </div>
      </div>
    </div>
</div>
</div>
</div>
@endsection
