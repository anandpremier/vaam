<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('commodity_id');
            $table->string('quantity')->nullable();
            $table->decimal('rate', 10, 2)->nullable();
            $table->decimal('net_amount', 10, 2)->nullable();
            $table->decimal('commission', 10, 2)->nullable();
            $table->decimal('transpotation', 10, 2)->nullable();
            $table->decimal('labour', 10, 2)->nullable();
            $table->decimal('gross_amount', 15, 2)->nullable();
            $table->text('note')->nullable();
            $table->string('purchase_invoice_no')->nullable();
            $table->tinyInteger('type')->unsigned()->default(0)->comment('1 - company , 0 - direct farmer');
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->text('address')->nullable();
            $table->string('mobile')->nullable();

            $table->foreign('commodity_id')->references('id')->on('commodity_mgmt')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('vendor')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase');
    }
}
