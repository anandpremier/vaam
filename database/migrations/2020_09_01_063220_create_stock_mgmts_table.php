<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockMgmtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_mgmts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('commodity_id');
            $table->unsignedBigInteger('commodity_category_id');

            $table->string('purchase_stock')->default(0);
            $table->unsignedBigInteger('purchase_id')->nullable();

            $table->string('production_stock')->default(0);
            $table->string('packed_stock')->default(0);
            $table->string('waste_stock')->default(0);

            $table->foreign('commodity_id')->references('id')->on('commodity_mgmt')->onDelete('cascade');
            $table->foreign('commodity_category_id')->references('id')->on('commodity_categories')->onDelete('cascade');
            $table->foreign('purchase_id')->references('id')->on('purchase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_mgmts');
    }
}
