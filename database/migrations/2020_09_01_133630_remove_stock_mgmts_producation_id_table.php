<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveStockMgmtsProducationIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_mgmts', function (Blueprint $table) {
            $table->dropForeign('stock_mgmts_purchase_id_foreign');
            $table->dropColumn('purchase_id');
            $table->dropColumn('producation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_mgmts', function (Blueprint $table) {
            //
        });
    }
}
