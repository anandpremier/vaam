<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('commodity_id');
            $table->unsignedBigInteger('commodity_category_id');
            $table->string('date')->nullable();
            $table->string('quantity')->nullable();
            $table->foreign('commodity_id')->references('id')->on('commodity_mgmt')->onDelete('cascade');
            $table->foreign('commodity_category_id')->references('id')->on('commodity_categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producations');
    }
}
