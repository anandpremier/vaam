<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\VaamUsers;
use Faker\Generator as Faker;

$factory->define(VaamUsers::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'mobile' => $faker->phoneNumber,
    ];
});
