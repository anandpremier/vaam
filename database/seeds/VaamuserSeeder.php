<?php

use Illuminate\Database\Seeder;

class VaamuserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\VaamUsers::class, 10)->create();

    }
}
